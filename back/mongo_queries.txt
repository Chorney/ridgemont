// /////////QUERY
//    {
//         "$match": {
//             "timestamp": {
//                 "$gt": 1502700719
//             }
//         }
//     },
//     {
//         "$limit": 1
//     },
//     {
//         "$project": {
//             "coins" : {
//                     "id": 1,
//                     "priceusd" :1,
//                     "pricebtc" :1

//             },
//             "timestamp": 1
//         }
//     },
//     {
//         "$unwind": "$coins"
//     },
//     {
//         "$match": {
//             "coins.id": {
//                 "$in": ["bitcoin", "ethereum","zcash"]
//             }
//         }
//     },
//     {
//         "$group": {
//             "_id": "$_id",
//             "coins": {
//                 "$addToSet": "$coins"
//             },
//             "timestamp": {"$last": "$timestamp"}
//         }
//     }

///////

// pipeline = []bson.M{
//  {
//      "$match": bson.M{
//          "timestamp": bson.M{
//              "$gt": 1502700719
//          }
//      }
//  },
//  {
//      "$limit": 1
//  },
//  {
//      "$unwind": "$coins"
//  },
//  {
//      "$match": bson.M{
//          "coins.id": bson.M{
//              "$in": ["bitcoin", "ethereum","zcash"]
//          }
//      }
//  },
//  {
//      "$group": bson.M{
//          "_id": "$_id",
//          "coins": bson.M{
//              "$addToSet": "$coins"
//          },
//          "timestamp": {"$last": "$timestamp"}
//      }
//  }
//  }

//query to get the names and symbols of the coins
{
"$sort": {"timestamp" : -1}
},
{
"$limit": 1
},
{
"$project": {
    "coins" : {
            "id": 1,
            "symbol" :1
    },
    "timestamp": 1
}
},
{
"$unwind": "$coins"
},
{
"$group": {
    "_id": "$_id",
    "coins": {
        "$addToSet": "$coins"
    },
}
}

db.runCommand({
	collMod: "cc_transactions",
	validator: { $and: [{name : {$exists: true}},{$and: [{quantity: {$exists: true}},{quantity: {$type: "number"}}]},{$and: [{timestamp: {$exists: true}},{quantity: {$type: "number"}}]}] },
	validationLevel: "strict"
}) 