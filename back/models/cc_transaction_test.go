package models

import (
	"testing"
	"gopkg.in/mgo.v2/bson"
)


func TestNewCcTransaction(t *testing.T) {

	expected_c_name := "testname"
	expected_qty := 123.123
	expected_timestamp := 123123123
	expected_id  := bson.NewObjectId()

	v := NewCcTransaction(expected_c_name, expected_qty, expected_timestamp, expected_id)

	if v.CName != expected_c_name {
		t.Errorf("Error setting model values for NewCCtransactions")
	}

	if v.QuantityHeld != expected_qty {
		t.Errorf("Error setting model values for NewCCtransactions")
	}

	if v.TransactionTime != expected_timestamp {
		t.Errorf("Error setting model values for NewCCtransactions")
	}

	if v.ID != expected_id {
		t.Errorf("Error setting model values for NewCCtransactions")
	}

}