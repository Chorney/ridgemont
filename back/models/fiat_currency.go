package models

type FiatCurrency struct {
	ID    string `json:"_id"`
	Base  string `json:"base"`
	Date  string `json:"date"`
	Rates struct {
		Aud float64 `json:"aud"`
		Cad float64 `json:"cad"`
		Chf float64 `json:"chf"`
		Cny float64 `json:"cny"`
		Gbp float64 `json:"gbp"`
		Inr float64 `json:"inr"`
		Jpy float64 `json:"jpy"`
		Krw float64 `json:"krw"`
		Mxn float64 `json:"mxn"`
		Php float64 `json:"php"`
		Rub float64 `json:"rub"`
		Eur float64 `json:"eur"`
	} `json:"rates"`
	Timestamp   int `json:"timestamp"`
	Btcusdvalue float64
	tableName   string
}
