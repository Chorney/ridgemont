package models

import (
	"time"
	// _"projects/cc-portfolio/cc-backend/config"
)

type User struct {
	Id				int
	UserName string
	Pwhash []byte
	First    string
	Last     string
	Role     string
	Admin			bool
	PortfolioPercent  float64
	Created_at	time.Time
	Last_logged_in time.Time
}
