package models

import (
	"gopkg.in/mgo.v2/bson"
)

const FIATTABLENAME = "fiat_transactions"

type FiatTransaction struct {
	Name      string        `json:"name" bson:"name"`
	Quantity  float64       `json:"quantity" bson:"quantity"`
	Timestamp int           `json:"timestamp" bson:"timestamp"`
	ID        bson.ObjectId `json:"id" bson:"_id"`
	// Id bson.ObjectId `json:"_id" bson:"_id"`
	tableName string
}

func NewFiatTransaction(name string, qty float64, time int, id bson.ObjectId) *FiatTransaction {
	cc_t := FiatTransaction{name, qty, time, id, FIATTABLENAME}
	return &cc_t
}

func NewFiatTransactions() []FiatTransaction {
	return []FiatTransaction{}
}

func (ft *FiatTransaction) AlterPortfolio(percent float64) {
	ft.Quantity = ft.Quantity * percent
}

func (ft FiatTransaction) IdToStr(id bson.ObjectId) string {
	return id.Hex()
}

func (ft FiatTransaction) GetTableName() string {
	return ft.tableName
}

func (ft FiatTransaction) GetId() bson.ObjectId {
	return ft.ID
}
