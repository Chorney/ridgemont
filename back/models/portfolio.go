package models

type Portfolio struct {
	PriceUsd  float64 `json:"priceusd"`
	PriceBtc  float64 `json:"pricebtc"`
	Timestamp int     `json:"timestamp"`

	// Date string `json:"date"`
}

func (pt *Portfolio) AlterPortfolio(percent float64) {
	pt.PriceBtc = pt.PriceBtc * percent
	pt.PriceUsd = pt.PriceUsd * percent
}
