package models

import (
	"gopkg.in/mgo.v2/bson"
)

const TABLENAME = "cc_transactions"

type Model interface {
	GetTableName() string
	GetId() bson.ObjectId
}

type CcTransaction struct {
	Name      string        `json:"name" bson:"name"`
	Quantity  float64       `json:"quantity" bson:"quantity"`
	Timestamp int           `json:"timestamp" bson:"timestamp"`
	ID        bson.ObjectId `json:"id" bson:"_id"`
	// Id bson.ObjectId `json:"_id" bson:"_id"`
	tableName string
}

func NewCcTransaction(name string, qty float64, time int, id bson.ObjectId) *CcTransaction {
	cc_t := CcTransaction{name, qty, time, id, TABLENAME}
	return &cc_t
}

func NewCcTransactions() []CcTransaction {
	return []CcTransaction{}
}

func (cct *CcTransaction) AlterPortfolio(percent float64) {
	cct.Quantity = cct.Quantity * percent
}

func (cct CcTransaction) IdToStr(id bson.ObjectId) string {
	return id.Hex()
}

func (cc CcTransaction) GetTableName() string {
	return cc.tableName
}

func (cc CcTransaction) GetId() bson.ObjectId {
	return cc.ID
}
