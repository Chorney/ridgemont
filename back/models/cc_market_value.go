package models

import "strconv"

//format for each coin on the remote server
type CcMarketValue struct {
	Name             string  `json:"name"`
	Symbol           string  `json:"symbol"`
	Rank             int     `json:"rank"`
	PriceUsd         float64 `json:"priceusd"`
	PriceBtc         float64 `json:"pricebtc"`
	Two4HVolumeUsd   float64 `json:"24hvolumeusd"`
	MarketCapUsd     float64 `json:"marketcapusd"`
	AvailableSupply  float64 `json:"availablesupply"`
	TotalSupply      float64 `json:"totalsupply"`
	PercentChange1H  float64 `json:"percentchange1h"`
	PercentChange24H float64 `json:"percentchange24h"`
	PercentChange7D  float64 `json:"percentchange7d"`
	LastUpdated      int     `json:"lastupdated"`
	// Date string `json:"date"`
}

//Creating new format structure for the ringbuffer locally
type Coin struct {
	Name             string `json:"id" bson:"id"` //stupid coinmarketcap naming convention
	Symbol           string `json:"symbol"`
	Rank             string `json:"rank"`
	PriceUsd         string `json:"priceusd"`
	PriceBtc         string `json:"pricebtc"`
	Two4HVolumeUsd   string `json:"two4hvolumeusd"`
	MarketCapUsd     string `json:"marketcapusd"`
	AvailableSupply  string `json:"availablesupply"`
	TotalSupply      string `json:"totalsupply"`
	PercentChange1H  string `json:"percentchange1h"`
	PercentChange24H string `json:"percentchange24h"`
	PercentChange7D  string `json:"percentchange7d"`
	LastUpdated      string `json:"lastupdated"`
}

type CcMarketValueNew struct {
	ID        int    `json:"_id"`
	Coins     []Coin `json:"coins"`
	Timestamp int    `json:"timestamp"`
}

func ConvertMarketValueNewOld(b []CcMarketValueNew) []CcMarketValue {

	var out []CcMarketValue

	for _, t := range b {
		rank, _ := strconv.Atoi(t.Coins[0].Rank)
		priceUsd, _ := strconv.ParseFloat(t.Coins[0].PriceUsd, 64)
		priceBtc, _ := strconv.ParseFloat(t.Coins[0].PriceBtc, 64)
		two4HVolumeUsd, _ := strconv.ParseFloat(t.Coins[0].Two4HVolumeUsd, 64)
		marketCapUsd, _ := strconv.ParseFloat(t.Coins[0].MarketCapUsd, 64)
		availableSupply, _ := strconv.ParseFloat(t.Coins[0].AvailableSupply, 64)
		totalSupply, _ := strconv.ParseFloat(t.Coins[0].TotalSupply, 64)
		percentChange1H, _ := strconv.ParseFloat(t.Coins[0].PercentChange1H, 64)
		percentChange24H, _ := strconv.ParseFloat(t.Coins[0].PercentChange24H, 64)
		percentChange7D, _ := strconv.ParseFloat(t.Coins[0].PercentChange7D, 64)
		lastupdated, _ := strconv.Atoi(t.Coins[0].LastUpdated)

		temp := CcMarketValue{t.Coins[0].Name, t.Coins[0].Symbol, rank, priceUsd, priceBtc, two4HVolumeUsd, marketCapUsd, availableSupply, totalSupply, percentChange1H, percentChange24H, percentChange7D, lastupdated}
		out = append(out, temp)
	}

	return out
}
