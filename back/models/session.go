package models

import (
	"time"
)

type Session struct {
	Un           string
	LastActivity time.Time
}

type UserInfo struct {
	Name             string
	Admin            int
	PortfolioPercent float64
}

type TokenMarshaller struct {
	IdToken string `json:"id_token"`
}

type IdTokenMarshaller struct {
	Sub   string `json:"sub"`
	Iss   string `json:"iss"`
	Exp   string `json:"exp"`
	Email string `json:"email"`
}
