package config

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Error struct {
	Message string
	Code    int
}

type ErrorNonEmpty struct {
	Message        string
	Code           int
	RequestPayload interface{}
}

func ResData(w http.ResponseWriter, response interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	js, _ := json.Marshal(response)
	fmt.Fprint(w, string(js), "\n")
}

func ResErrorEmptyReqPayload(w http.ResponseWriter, code int, message string) {
	w.WriteHeader(code)

	response := Error{Code: code, Message: message}
	js, _ := json.Marshal(response)
	fmt.Fprint(w, string(js), "\n")
}

func ResErrorNonEmptyReqPayload(w http.ResponseWriter, code int, message string, payload interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	response := ErrorNonEmpty{Code: code, Message: message, RequestPayload: payload}
	js, _ := json.Marshal(response)
	fmt.Fprint(w, string(js), "\n")
}
