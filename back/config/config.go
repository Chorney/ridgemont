package config

import (
	"projects/ridgemont/back/db"
	"projects/ridgemont/back/models"
	"time"

	_ "github.com/lib/pq"
)

const SESSIONLENGTH int = 200000
const TEMPLATEMODE bool = true
const PORTFOLIOSTART int = 1451606400
const LOCALCCDATABASENAME string = "cc_database"
const REMOTECCDATABASENAME string = "cc_databasev2"

type Config struct {
	SqlDb             *db.SqlDbLocal
	MongoDbLocal      db.RestfulDb
	MongoDbRemote     *db.MongoDbRemote
	DbSessions        map[string]models.Session
	DbSessionsCleaned time.Time
	Ringbufferaddress string
	DbUsers           map[string]models.UserInfo
}

var Options FlagConfig

type FlagConfig struct {
	ProductionFlag string
	TemplateMode   string
	UnlockedMode   string
	DebugMode      string
	OauthMode      string
}

func NewConfig() *Config {
	// db, _ := sql.Open("postgres", "postgres://starrover@localhost:5432/cc_database?sslmode=disable")

	//sql database initialization
	// DB := connectDbSql()

	//mongodb database initialization
	// DB1 := connectDbMongo("mongodb://192.168.1.52/")
	// DB2 := connectDbMongoCal("mongodb://uk_cc_client:rrwrF9YGfBSCfh839@192.168.1.52:27018/cc_databasev2")

	SqlDb_ := db.NewSqlDbLocal("asdasd")
	MongoDbLocal_ := db.NewMongoDbLocal("mongodb://db-coins/")
	MongoDbRemote_ := db.NewMongoDbRemote("mongodb://uk_cc_client:rrwrF9YGfBSCfh839@db-jump:27018/cc_databasev2")

	//Cookie sessions
	DbSessions := map[string]models.Session{}
	DbSessionsCleaned := time.Now()
	DbUsers := map[string]models.UserInfo{}

	//Drew
	DbUsers["114601271909099517829"] = models.UserInfo{"Drew", 3, 0.475}
	DbUsers["116672854234340590721"] = models.UserInfo{"Starrover", 1, 0.1}

	var address string
	if Options.ProductionFlag == "true" {
		address = "http://ringbuffer:8082"
	} else {
		address = "http://localhost:8082"
	}

	return &Config{SqlDb_, MongoDbLocal_, MongoDbRemote_, DbSessions, DbSessionsCleaned, address, DbUsers}
}
