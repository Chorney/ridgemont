package ccapphelpers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/models"
	"time"
)

//For Get functino query parameter
func AlreadyGoogleLoggedIn(cfg *config.Config, w http.ResponseWriter, r *http.Request) (int, *models.UserInfo, error) {

	// body, err := ioutil.ReadAll(r.Body)
	// defer r.Body.Close()
	// if err != nil {
	// 	errmsg := "helper, action AlreadyGoogleLoggedIn: Error reading request body"
	// 	if config.Options.DebugMode == "true" {
	// 		fmt.Println(err)
	// 		fmt.Println(errmsg)
	// 	}
	// 	return 0, nil, fmt.Errorf(errmsg)
	// }

	// var t models.TokenMarshaller
	// err = json.Unmarshal(body, &t)
	// if err != nil {
	// 	errmsg := "helper, action AlreadyGoogleLoggedIn: Error unmarshalling request body from json"
	// 	if config.Options.DebugMode == "true" {
	// 		fmt.Println(err)
	// 		fmt.Println(errmsg)
	// 	}
	// 	return 0, nil, fmt.Errorf(errmsg)
	// }

	id_token := r.FormValue("id_token")

	response, err := http.Get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + id_token)
	if config.Options.DebugMode == "true" {
		fmt.Printf("#v", response)
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)

	var idt models.IdTokenMarshaller
	err = json.Unmarshal(contents, &idt)

	if err != nil {
		errmsg := "helper, action AlreadyGoogleLoggedIn: Error unmarshalling request body from json"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		return 0, nil, fmt.Errorf(errmsg)
	}

	if idt.Iss != "accounts.google.com" && idt.Iss != "https://accounts.google.com" {
		errmsg := "helper, action AlreadyGoogleLoggedIn: Wrong Issuer, token is not from google"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusForbidden, errmsg)
		return 0, nil, fmt.Errorf(errmsg)
	}

	if _, ok := cfg.DbUsers[idt.Sub]; !ok {
		errmsg := "sessions, action GoogleLogin: Not an Accepted User ID"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusForbidden, errmsg)
		return 0, nil, fmt.Errorf(errmsg)
	}

	val := cfg.DbUsers[idt.Sub]

	fmt.Println(val.Admin)

	return val.Admin, &val, nil
}

func AlreadyLoggedIn(cfg *config.Config, w http.ResponseWriter, req *http.Request) (int, models.User) {

	var status int
	user := models.User{}

	c, err := req.Cookie("cc_session")
	if err != nil {
		return 0, user
	}
	s, ok := cfg.DbSessions[c.Value]
	if ok {
		s.LastActivity = time.Now()
		cfg.DbSessions[c.Value] = s
	} else {
		return 0, user
	}
	// _, ok = config.DbUsers[s.Un]

	row := cfg.SqlDb.GetDb().QueryRow("SELECT * FROM users WHERE username = $1", s.Un)
	err = row.Scan(&user.Id, &user.UserName, &user.Pwhash, &user.First, &user.Last, &user.Role, &user.PortfolioPercent, &user.Created_at, &user.Last_logged_in) // order matters
	if err != nil {
		return 0, user
	}

	// refresh session
	c.MaxAge = config.SESSIONLENGTH
	if config.Options.TemplateMode == "false" {
		c.Path = "/"
	} else {
		c.Path = "/api/"
	}
	http.SetCookie(w, c)

	fmt.Println("--------------")
	fmt.Println(user.Role)
	fmt.Println("--------------")

	switch user.Role {
	case "user":
		status = 1
	case "manager":
		status = 2
	case "admin":
		status = 3
	default:
		status = 0
	}

	return status, user
}

//TODO: IsADmin not working
func IsAdmin(cfg *config.Config, w http.ResponseWriter, req *http.Request) bool {
	status, user := AlreadyLoggedIn(cfg, w, req)
	if status != 0 && user.Admin == true {
		return true
	} else {
		return false
	}

}

func CleanSessions(cfg *config.Config) {
	fmt.Println("BEFORE CLEAN") // for demonstration purposes
	// showSessions()              // for demonstration purposes
	for k, v := range cfg.DbSessions {
		if time.Now().Sub(v.LastActivity) > (time.Second * 30) {
			delete(cfg.DbSessions, k)
		}
	}
	cfg.DbSessionsCleaned = time.Now()
	fmt.Println("AFTER CLEAN") // for demonstration purposes
	// showSessions()             // for demonstration purposes
}

// for demonstration purposes
func ShowSessions(cfg *config.Config) {
	fmt.Println("********")
	for k, v := range cfg.DbSessions {
		fmt.Println(k, v.Un)
	}
	fmt.Println("")
}
