package arrayhelpers

import "testing"

func TestInArray(t *testing.T) {

	word1 := "word1"
	slice := []string{"word2", "word3", "word4"}

	yes, _ := InArray(word1, slice)

	if yes != false {
		t.Errorf("Package arrayhelpers function InArray not working, expected false")
	}

	word1 = "word3"

	yes, index := InArray(word1, slice)

	if yes != true && index != 1 {
		t.Errorf("Package arrayhelpers function InArray not working, expected true")
	}

}
