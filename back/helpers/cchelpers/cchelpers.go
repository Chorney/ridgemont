package cchelpers

import (
	"errors"
	"fmt"
	"projects/ridgemont/back/models"
	"sort"
	"strconv"

	"gopkg.in/mgo.v2/bson"
)

type CoinSlice []models.Coin

type CcSummaryElement struct {
	Name             string        `json:"name" bson:"name"`
	Quantity         float64       `json:"quantity" bson:"quantity"`
	Timestamp        int           `json:"timestamp" bson:"timestamp"`
	ID               bson.ObjectId `json:"id" bson:"_id"`
	Symbol           string        `json:"symbol"`
	Rank             int           `json:"rank"`
	PriceUsd         float64       `json:"priceusd"`
	PriceBtc         float64       `json:"pricebtc"`
	Two4HVolumeUsd   float64       `json:"two4hvolumeusd"`
	MarketCapUsd     float64       `json:"marketcapusd"`
	AvailableSupply  float64       `json:"availablesupply"`
	TotalSupply      float64       `json:"totalsupply"`
	PercentChange1H  float64       `json:"percentchange1h"`
	PercentChange24H float64       `json:"percentchange24h"`
	PercentChange7D  float64       `json:"percentchange7d"`
	LastUpdated      int           `json:"lastupdated"`
}

func (cc_elem *CcSummaryElement) SetQuantity(value float64) {
	cc_elem.Quantity = cc_elem.Quantity * value
}

// Id bson.ObjectId `json:"_id" bson:"_id"`

type FiatSummaryElement struct {
	Name      string        `json:"name" bson:"c_name"`
	Quantity  float64       `json:"quantity" bson:"quantity"`
	Timestamp int           `json:"timestamp" bson:"timestamp"`
	ID        bson.ObjectId `json:"id" bson:"_id"`
	PriceUsd  float64       `json:"priceusd"`
	PriceBtc  float64       `json:"pricebtc"`
}

func (slice CoinSlice) Len() int {
	return len(slice)
}

func (slice CoinSlice) Less(i, j int) bool {
	return slice[i].Name < slice[j].Name
}

func (slice CoinSlice) Swap(i, j int) {
	slice[i], slice[j] = slice[j], slice[i]
}

func GetProfileValues(u []models.CcTransaction, v models.CcMarketValueNew) (float64, float64, error) {

	//Function returns val_us,val_btc
	var s CoinSlice
	var val_us, val_btc float64
	var err error

	s = v.Coins

	// var length1 = len(u)
	// var length2 = len(v.Coins)
	if len(u) != len(s) {
		err = errors.New("Error with unmatching slice lengths")
		// log.Fatalln(err)
		// fmt.Println("Error with unmatching string types")
	}

	sort.Sort(s)

	for i := 0; i < len(s); i++ {
		if u[i].Name != s[i].Name {
			err = errors.New("Error with uneqaul coin names/ids")
		}

		p_us, _ := strconv.ParseFloat(s[i].PriceUsd, 64)
		p_btc, _ := strconv.ParseFloat(s[i].PriceBtc, 64)
		val_us += u[i].Quantity * p_us
		val_btc += u[i].Quantity * p_btc
	}

	return val_us, val_btc, err
}

func GetPortfolioFiatValues(u []models.FiatTransaction, v models.FiatCurrency, btc_usd_val float64) (float64, float64) {

	var val_us, val_btc float64

	for _, t := range u {

		switch t.Name {
		case "cad":
			val_us = val_us + t.Quantity*(1/v.Rates.Cad)
		case "eur":
			val_us = val_us + t.Quantity*(1/v.Rates.Eur)
		case "gbp":
			val_us = val_us + t.Quantity*(1/v.Rates.Gbp)
		case "jpy":
			val_us = val_us + t.Quantity*(1/v.Rates.Jpy)
		case "aud":
			val_us = val_us + t.Quantity*(1/v.Rates.Aud)
		case "chf":
			val_us = val_us + t.Quantity*(1/v.Rates.Chf)
		case "cny":
			val_us = val_us + t.Quantity*(1/v.Rates.Cny)
		case "inr":
			val_us = val_us + t.Quantity*(1/v.Rates.Inr)
		case "krw":
			val_us = val_us + t.Quantity*(1/v.Rates.Krw)
		case "mxn":
			val_us = val_us + t.Quantity*(1/v.Rates.Mxn)
		case "php":
			val_us = val_us + t.Quantity*(1/v.Rates.Php)
		case "rub":
			val_us = val_us + t.Quantity*(1/v.Rates.Rub)
		case "usd":
			val_us = val_us + t.Quantity
		default:
			fmt.Println("currency selected not in available currencies", t.Name)
		}
	}
	val_btc = val_us / btc_usd_val

	return val_us, val_btc
}

func GetCoinSlice(u []models.CcTransaction) []string {

	var slice []string

	for i := 0; i < len(u); i++ {
		slice = append(slice, u[i].Name)
	}

	return slice
}

func JoinTrsCoin(u models.CcTransaction, v models.CcMarketValue) CcSummaryElement {

	new_ := CcSummaryElement{}

	new_.Name = u.Name
	new_.Quantity = u.Quantity
	new_.Timestamp = u.Timestamp
	new_.ID = u.ID
	new_.Name = v.Name
	new_.Symbol = v.Symbol
	new_.Rank = v.Rank
	new_.PriceUsd = v.PriceUsd
	new_.PriceBtc = v.PriceBtc
	new_.Two4HVolumeUsd = v.Two4HVolumeUsd
	new_.MarketCapUsd = v.MarketCapUsd
	new_.AvailableSupply = v.AvailableSupply
	new_.TotalSupply = v.TotalSupply
	new_.PercentChange1H = v.PercentChange1H
	new_.PercentChange24H = v.PercentChange24H
	new_.PercentChange7D = v.PercentChange7D
	new_.LastUpdated = v.LastUpdated

	return new_
}
