package main

import (
	"flag"
	"html/template"
	"net/http"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/controllers"
	"projects/ridgemont/back/helpers"

	"github.com/julienschmidt/httprouter"
	// "reflect"
	"fmt"
	"os"
)

var tpl *template.Template

func processFlags() {

	flag.StringVar(&config.Options.ProductionFlag, "production", "false", "When in production mode uses other config file variables (default false)")
	flag.StringVar(&config.Options.TemplateMode, "tplmode", "true", "Template mode means you can only log in by sending a post, and a number of other options")
	flag.StringVar(&config.Options.UnlockedMode, "unlockedmode", "false", "If unlocked mode == true than all pages are open, and no need to login, default is false")
	flag.StringVar(&config.Options.DebugMode, "debugmode", "true", "When debug mode is true, errors and messages are printed to the terminal (default true)")
	flag.StringVar(&config.Options.OauthMode, "oauthmode", "false", "When o-auth mode is true, authentication is through google oauth server (default false)")
	flag.Parse()
}

func main() {

	processFlags()

	if config.Options.TemplateMode == "true" {
		fmt.Println("running in Template Mode")
	}

	if config.Options.ProductionFlag == "true" {
		fmt.Println("running in Production mode")
	}
	if config.Options.UnlockedMode == "true" {
		fmt.Println("running in Unlocked Mode")
	}
	if config.Options.DebugMode == "true" {
		fmt.Println("running in Debug Mode")
	}
	if config.Options.OauthMode == "true" {
		fmt.Println("running in oauth mode")
	}

	cfg := config.NewConfig()

	cc := controllers.NewCcController(cfg)
	uc := controllers.NewUserController(cfg)
	sc := controllers.NewSessionController(cfg)
	trc := controllers.NewCcTransactionController(cfg)
	ft := controllers.NewFiatCurrencyController(cfg)
	fc := controllers.NewFiatController(cfg)
	vc := controllers.NewVisualizeController(cfg)

	//
	r := httprouter.New()
	r.GET("/", RunIndex)

	if config.Options.OauthMode == "false" {

		//**// cryptocurrency controller stuff
		r.GET("/api/coin/index", MakeUserLockedHandler(MakeHandler(cc.Index), cc))
		r.GET("/api/coin/types", MakeUserLockedHandler(MakeHandler(cc.GetAvailableCoins), cc))

		//**// Transaction controllers
		r.GET("/api/coin/transactions/read/:coin", MakeUserLockedHandler(MakeHandler(trc.GetCoin), trc))
		r.GET("/api/coin/transactions/create", MakeUserAdminLockedHandler(MakeHandler(trc.New), trc))
		r.GET("/api/coin/transactions/index", MakeUserLockedHandler(MakeHandler(trc.Index), trc))
		r.PUT("/api/coin/transactions/create", MakeUserAdminLockedHandler(MakeHandler(trc.Create), trc))
		r.OPTIONS("/api/coin/transactions/create", OptionsHandler)
		r.OPTIONS("/api/coin/transactions/delete", OptionsHandler)
		if config.Options.TemplateMode == "false" {
			r.DELETE("/api/coin/transactions/delete", MakeUserAdminLockedHandler(MakeHandler(trc.Delete), trc))
		} else {
			r.GET("/api/coin/transactions/delete", MakeUserAdminLockedHandler(MakeHandler(trc.Delete), trc))
		}

		//**// fiatcurrency controller stuff
		r.GET("/api/fiat/index", MakeUserLockedHandler(MakeHandler(fc.Index), fc))
		r.GET("/api/fiat/types", MakeUserLockedHandler(MakeHandler(fc.GetAvailableCoins), fc))

		//**// Fiat Transaction controllers
		r.GET("/api/fiat/transactions/read/:coin", MakeUserLockedHandler(MakeHandler(ft.GetFiat), ft))
		r.GET("/api/fiat/transactions/create", MakeUserAdminLockedHandler(MakeHandler(ft.New), ft))
		r.GET("/api/fiat/transactions/index", MakeUserLockedHandler(MakeHandler(ft.Index), ft))
		r.PUT("/api/fiat/transactions/create", MakeUserAdminLockedHandler(MakeHandler(ft.Create), ft))
		r.OPTIONS("/api/fiat/transactions/create", OptionsHandler)
		r.OPTIONS("/api/fiat/transactions/delete", OptionsHandler)
		if config.Options.TemplateMode == "false" {
			r.DELETE("/api/fiat/transactions/delete", MakeUserAdminLockedHandler(MakeHandler(ft.Delete), ft))
		} else {
			r.GET("/api/fiat/transactions/delete", MakeUserAdminLockedHandler(MakeHandler(ft.Delete), ft))
		}

		//**// visualization controller
		r.GET("/api/visualize/index", MakeUserLockedHandler(MakeHandler(vc.NewIndex), vc))
		r.POST("/api/visualize/index", MakeUserLockedHandler(MakeHandler(vc.Index), vc))
		r.GET("/api/visualize/read/:coin", MakeUserLockedHandler(MakeHandler(vc.GetCoin), vc))

		//*//users controller stuff
		r.GET("/api/users/signup", uc.New)
		r.POST("/api/users/signup", uc.Create)
		r.GET("/api/users/index", uc.Index)
		r.GET("/api/users/read/:num", uc.Show)
		r.DELETE("/api/users/read/:num", uc.Destroy)

		//**// Sessions controller stuff
		r.GET("/api/session/login", MakeHandler(sc.New))
		r.POST("/api/session/login", MakeHandler(sc.Create))
		r.OPTIONS("/api/session/login", OptionsHandler)
		r.GET("/api/session/logout", MakeHandler(sc.Destroy))
		r.OPTIONS("/api/session/logout", OptionsHandler)

	} else {
		//**// cryptocurrency controller stuff
		r.GET("/api/coin/index", MakeUserGoogleLockedHandler(MakeHandler(cc.Index), cc))
		r.GET("/api/coin/types", MakeUserGoogleLockedHandler(MakeHandler(cc.GetAvailableCoins), cc))

		//**// Transaction controllers
		r.GET("/api/coin/transactions/read/:coin", MakeUserGoogleLockedHandler(MakeHandler(trc.GetCoin), trc))
		r.GET("/api/coin/transactions/create", MakeUserAdminGoogleLockedHandler(MakeHandler(trc.New), trc))
		r.GET("/api/coin/transactions/index", MakeUserGoogleLockedHandler(MakeHandler(trc.Index), trc))
		r.PUT("/api/coin/transactions/create", MakeUserAdminGoogleLockedHandler(MakeHandler(trc.Create), trc))
		r.OPTIONS("/api/coin/transactions/create", OptionsHandler)
		r.OPTIONS("/api/coin/transactions/delete", OptionsHandler)
		if config.Options.TemplateMode == "false" {
			r.DELETE("/api/coin/transactions/delete", MakeUserAdminGoogleLockedHandler(MakeHandler(trc.Delete), trc))
		} else {
			r.GET("/api/coin/transactions/delete", MakeUserAdminGoogleLockedHandler(MakeHandler(trc.Delete), trc))
		}

		//**// fiatcurrency controller stuff
		r.GET("/api/fiat/index", MakeUserGoogleLockedHandler(MakeHandler(fc.Index), fc))
		r.GET("/api/fiat/types", MakeUserGoogleLockedHandler(MakeHandler(fc.GetAvailableCoins), fc))

		//**// Fiat Transaction controllers
		r.GET("/api/fiat/transactions/read/:coin", MakeUserGoogleLockedHandler(MakeHandler(ft.GetFiat), ft))
		r.GET("/api/fiat/transactions/create", MakeUserAdminGoogleLockedHandler(MakeHandler(ft.New), ft))
		r.GET("/api/fiat/transactions/index", MakeUserGoogleLockedHandler(MakeHandler(ft.Index), ft))
		r.PUT("/api/fiat/transactions/create", MakeUserAdminGoogleLockedHandler(MakeHandler(ft.Create), ft))
		r.OPTIONS("/api/fiat/transactions/create", OptionsHandler)
		r.OPTIONS("/api/fiat/transactions/delete", OptionsHandler)
		if config.Options.TemplateMode == "false" {
			r.DELETE("/api/fiat/transactions/delete", MakeUserAdminGoogleLockedHandler(MakeHandler(ft.Delete), ft))
		} else {
			r.GET("/api/fiat/transactions/delete", MakeUserAdminGoogleLockedHandler(MakeHandler(ft.Delete), ft))
		}

		//**// visualization controller
		r.GET("/api/visualize/index", MakeUserGoogleLockedHandler(MakeHandler(vc.NewIndex), vc))
		r.POST("/api/visualize/index", MakeUserGoogleLockedHandler(MakeHandler(vc.Index), vc))
		r.GET("/api/visualize/read/:coin", MakeUserGoogleLockedHandler(MakeHandler(vc.GetCoin), vc))

		r.OPTIONS("/api/session/logout", OptionsHandler)

		r.GET("/api/session/googlelogintemplate", sc.GoogleLoginTemplateMode)
		r.GET("/api/session/googlecallback", sc.GoogleCallback)

		r.POST("/api/session/login", sc.GoogleLogin)
		r.DELETE("/api/session/logout", sc.GoogleLogout)
	}

	fmt.Println(os.Getenv("CCPATH") + "/back/public/static/")

	// http.Handle("/", http.FileServer(http.Dir("/home/starrover/Dev/gocode/src/projects/cc-portfolio/cc-backend/")))
	r.ServeFiles("/static/*filepath", http.Dir(os.Getenv("CCPATH")+"/public/static/"))
	// r.ServeFiles("/styles.css/*filepath", http.Dir("/home/starrover/Dev/gocode/src/projects/cc-portfolio/cc-backend"))
	// r.ServeFiles("/static/css/*filepath", http.Dir("/home/starrover/Dev/gocode/src/projects/cc-portfolio/cc-backend"))
	http.ListenAndServe(":8080", r)
}

func MakeUserLockedHandler(fn func(w http.ResponseWriter, r *http.Request, p httprouter.Params), ctrl controllers.Ctrl) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if config.Options.UnlockedMode == "false" {
			status, _ := ccapphelpers.AlreadyLoggedIn(ctrl.GetConfig(), w, r)
			fmt.Println(r.FormValue("header"))
			if status == 0 {
				w.WriteHeader(http.StatusForbidden)
				return
			}
		}
		fn(w, r, p)
	}
}

//MakeUserGoogleLockedHandler is locking down general user areas with google oauth server
func MakeUserGoogleLockedHandler(fn func(w http.ResponseWriter, r *http.Request, p httprouter.Params), ctrl controllers.Ctrl) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if config.Options.UnlockedMode == "false" {
			status, _, _ := ccapphelpers.AlreadyGoogleLoggedIn(ctrl.GetConfig(), w, r)
			fmt.Println(r.FormValue("header"))
			if status == 0 {
				w.WriteHeader(http.StatusForbidden)
				return
			}
		}
		fn(w, r, p)
	}
}

func MakeUserAdminGoogleLockedHandler(fn func(w http.ResponseWriter, r *http.Request, p httprouter.Params), ctrl controllers.Ctrl) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if config.Options.UnlockedMode == "false" {
			status, _, _ := ccapphelpers.AlreadyGoogleLoggedIn(ctrl.GetConfig(), w, r)
			fmt.Println(r.FormValue("header"))
			if status != 3 {
				w.WriteHeader(http.StatusForbidden)
				return
			}
		}
		fn(w, r, p)
	}
}

func MakeUserAdminLockedHandler(fn func(w http.ResponseWriter, r *http.Request, p httprouter.Params), ctrl controllers.Ctrl) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		if config.Options.UnlockedMode == "false" {
			status, _ := ccapphelpers.AlreadyLoggedIn(ctrl.GetConfig(), w, r)
			fmt.Println(r.FormValue("header"))
			if status != 3 {
				w.WriteHeader(http.StatusForbidden)
				return
			}
		}
		fn(w, r, p)
	}
}

func MakeHandler(fn func(w http.ResponseWriter, r *http.Request, p httprouter.Params)) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		w.Header().Set("Access-Control-Allow-Origin", "*")

		if config.Options.TemplateMode == "false" {
			w.Header().Set("Content-Type", "application/json")
		}
		fn(w, r, p)
	}
}

func OptionsHandler(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	if origin := r.Header.Get("Origin"); origin != "" {
		w.Header().Set("Access-Control-Allow-Origin", origin)
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}
}

func RunIndex(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	fmt.Println("You've entered the index")

	tpl = template.Must(template.ParseFiles("public/index.html"))
	tpl.ExecuteTemplate(w, "index.html", nil)
}
