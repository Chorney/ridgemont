package db

import (
	"fmt"
	"log"
	"projects/ridgemont/back/models"
	"strconv"
	"time"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const REMOTECCDATABASENAME string = "cc_databasev2"
const DEBUG = false

type MongoDbRemote struct {
	db *mgo.Database
}

func NewMongoDbRemote(server string) *MongoDbRemote {
	db_ := MongoDbRemote{}
	db_.ConnectDb(server)
	return &db_
}

func (db *MongoDbRemote) ConnectDb(server string) {
	//Connect to our local mongo
	s, err := mgo.Dial(server)

	// Check if connection error, is mongo running ?
	if err != nil {
		if DEBUG == true {
			fmt.Println("Trouble connecting to remote mongodb server")
		}
		log.Fatalln(err)
		panic(err)
	}

	if err = s.Ping(); err != nil {
		if DEBUG == true {
			fmt.Println("Trouble pining the remote mongodb server")
		}
		log.Fatalln(err)
		panic(err)
	}

	fmt.Println("You connected to your Remote Mongo database.")

	DB := s.DB(REMOTECCDATABASENAME)

	db.db = DB
}

func (db MongoDbRemote) GetDb() *mgo.Database {
	return db.db
}

func (db MongoDbRemote) getMarketValuesAtTime(time_ int, coins []string) models.CcMarketValueNew {

	pipeline := []bson.M{
		{
			"$match": bson.M{
				"timestamp": bson.M{
					"$gt": time_,
				}}},
		{
			"$limit": 1,
		},
		{
			"$unwind": "$coins",
		},
		{
			"$match": bson.M{
				"coins.id": bson.M{
					"$in": coins,
				}}},
		{
			"$group": bson.M{
				"_id": "$_id",
				"coins": bson.M{
					"$addToSet": "$coins",
				},
				"timestamp": bson.M{"$last": "$timestamp"},
			}},
	}

	b := models.CcMarketValueNew{}
	pipe := db.GetDb().C("coins").Pipe(pipeline)
	err := pipe.One(&b)
	if err != nil {
		fmt.Println("Error with profile building pipeline")
		log.Fatalln(err)
	}

	return b
}

func (db MongoDbRemote) GetTimeEarliest() int {
	vu := models.Portfolio{}
	db.GetDb().C("coins").Find(bson.M{}).Sort("timestamp").Limit(1).One(&vu)

	return vu.Timestamp
}

func (db MongoDbRemote) GetTimeLatest() int {
	vu := models.Portfolio{}
	db.GetDb().C("coins").Find(bson.M{}).Sort("-timestamp").Limit(1).One(&vu)

	return vu.Timestamp
}

func (db MongoDbRemote) GetFiatValuesAtTime(time_ int) models.FiatCurrency {

	v := models.FiatCurrency{}
	err := db.GetDb().C("fiat").Find(bson.M{"timestamp": bson.M{"$gt": time_}}).Sort("+timestamp").Limit(1).One(&v)
	if err != nil {
		err := db.GetDb().C("fiat").Find(bson.M{"timestamp": bson.M{"$lt": time_}}).Sort("-timestamp").Limit(1).One(&v)
		if err != nil {
			fmt.Println(err)
			fmt.Println("having issues find from database")
		}
	}
	return v
}
func (db MongoDbRemote) GetFiatMostRecentValue() models.FiatCurrency {

	v := models.FiatCurrency{}
	db.GetDb().C("fiat").Find(bson.M{}).Sort("-timestamp").Limit(1).One(&v)
	return v
}

func (db MongoDbRemote) GetValueBTCNow() float64 {
	time_now := int(time.Now().Unix())

	val := db.GetValueBTC(time_now)
	return val
}

func (db MongoDbRemote) GetValueBTC(time_ int) float64 {

	pipeline := []bson.M{
		{
			"$match": bson.M{
				"timestamp": bson.M{
					"$gt": time_,
				}}},
		{
			"$limit": 1,
		},
		{
			"$unwind": "$coins",
		},
		{
			"$match": bson.M{
				"coins.id": "bitcoin",
			}},
		{
			"$group": bson.M{
				"_id": "$_id",
				"coins": bson.M{
					"$addToSet": "$coins",
				},
				"timestamp": bson.M{"$last": "$timestamp"},
			}},
	}

	b := models.CcMarketValueNew{}
	pipe := db.GetDb().C("coins").Pipe(pipeline)
	err := pipe.One(&b)
	if err != nil {

		pipeline2 := []bson.M{
			{
				"$match": bson.M{
					"timestamp": bson.M{
						"$lte": time_,
					}}},
			{
				"$sort": bson.M{"timestamp": -1},
			},
			{
				"$limit": 1,
			},
			{
				"$unwind": "$coins",
			},
			{
				"$match": bson.M{
					"coins.id": "bitcoin",
				}},
			{
				"$group": bson.M{
					"_id": "$_id",
					"coins": bson.M{
						"$addToSet": "$coins",
					},
					"timestamp": bson.M{"$last": "$timestamp"},
				}},
		}

		pipe := db.GetDb().C("coins").Pipe(pipeline2)
		err := pipe.One(&b)
		if err != nil {
			fmt.Println("Could not find the most recent value of bitcoin in USD")
		}
	}

	// fmt.Println(b.Coins[0].PriceUsd)

	price_btc, _ := strconv.ParseFloat(b.Coins[0].PriceUsd, 64)
	return price_btc

}

type CoinListMarshaller struct {
	ID    string `json:"_id"`
	Coins []struct {
		Name   string `json:"name" bson:"id"`
		Symbol string `json:"symbol"`
		Rank   string `json:"rank"`
	} `json:"coins"`
}

func (db MongoDbRemote) GetRemoteCoinList() CoinListMarshaller {

	u := CoinListMarshaller{}

	pipeline := []bson.M{
		{
			"$sort": bson.M{"timestamp": 1},
		},
		{
			"$limit": 1,
		},
		{
			"$project": bson.M{
				"coins": bson.M{
					"id":     1,
					"symbol": 1,
					"rank":   1,
				},
				"timestamp": 1,
			}},
		{
			"$unwind": "$coins",
		},
		{
			"$group": bson.M{
				"_id": "$_id",
				"coins": bson.M{
					"$addToSet": "$coins",
				}}},
	}

	pipe := db.GetDb().C("coins").Pipe(pipeline)
	err := pipe.One(&u)

	if err != nil {
		fmt.Println("Error finding the list of coins and symobls from the remote database")
	}

	return u

}

func (db MongoDbRemote) GetRemoteCoinNames() []string {

	blah := db.GetRemoteCoinList().Coins
	var coinnames []string

	for _, item := range blah {
		coinnames = append(coinnames, item.Name)
	}

	return coinnames
}
