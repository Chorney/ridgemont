package db

import "testing"

func TestSqlConnection(t *testing.T) {

	DB := NewSqlDbLocal("blah")

	err := DB.GetDb().Ping()

	if err != nil {
		t.Errorf("Issues connecting to the remote database")
	}
}
