package db

import (
	"fmt"
	"log"
	"projects/ridgemont/back/models"
	"reflect"
	"testing"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const MONGOCONNECTION = "mongodb://192.168.1.52/"

func TestDataBaseConnection(t *testing.T) {

	DB := NewMongoDbLocal(MONGOCONNECTION)

	_, err := DB.GetDb().CollectionNames()

	if err != nil {
		t.Errorf("Issues connecting to the database")
	}
}

type fakeModel struct {
	CName           string        `json:"c_name" bson:"c_name"`
	QuantityHeld    float64       `json:"quantity_held" bson:"quantity_held"`
	TransactionTime int           `json:"transactiontime" bson:"transactiontime"`
	ID              bson.ObjectId `json:"id" bson:"_id"`
	// Id bson.ObjectId `json:"_id" bson:"_id"`
	tableName string
}

func (cc fakeModel) GetTableName() string {
	return cc.tableName
}

func (cc fakeModel) GetId() bson.ObjectId {
	return cc.ID
}

func TestCreateDelete(t *testing.T) {

	DB := NewMongoDbLocal(MONGOCONNECTION)

	m := fakeModel{"canadian", 10.0, 123238, bson.NewObjectId(), "cc_test"}
	err := DB.Create(m)
	if err != nil {
		t.Errorf("Issues inserting model into the database")
	}

	err = DB.Delete(m)
	if err != nil {
		log.Fatalln(err)
		t.Errorf("Issues deleting model from the database")
	}

	DB.GetDb().C("cc_test").DropCollection()
}

func TestIndex(t *testing.T) {

	DB := NewMongoDbLocal(MONGOCONNECTION)
	DB.GetDb().C("cc_test").DropCollection()

	testCases := []struct {
		CName           string        `json:"c_name" bson:"c_name"`
		QuantityHeld    float64       `json:"quantity_held" bson:"quantity_held"`
		TransactionTime int           `json:"transactiontime" bson:"transactiontime"`
		ID              bson.ObjectId `json:"id" bson:"_id"`
	}{
		{
			CName:           "canadian",
			QuantityHeld:    12.0,
			TransactionTime: 12312312,
			ID:              bson.NewObjectId(),
		},
		{
			CName:           "canadian",
			QuantityHeld:    12.0,
			TransactionTime: 12312123,
			ID:              bson.NewObjectId(),
		},
		{
			CName:           "canadian",
			QuantityHeld:    12.0,
			TransactionTime: 23423422,
			ID:              bson.NewObjectId(),
		},
	}

	err := DB.GetDb().C("cc_test").Insert(testCases[0])
	err = DB.GetDb().C("cc_test").Insert(testCases[1])
	err = DB.GetDb().C("cc_test").Insert(testCases[2])
	// expectedCount := 3

	if err != nil {
		t.Errorf("Trouble inserting test data into the database")
	}

	var blah models.CcTransaction
	out, err := DB.Index(blah, "cc_test")
	if err != nil {
		t.Errorf("Trouble retrieving transactions from the database")
	}
	fmt.Println(reflect.TypeOf(out))
	// if len(out) != expectedCount {
	// 	t.Errorf("Count of inserted documents does not match output from index")
	// }

	DB.GetDb().C("cc_test").DropCollection()
}

func TestUpdatePortfolioAtTime(t *testing.T) {
	fmt.Println("--Testing UpdatePortfolioAtTime--")
	DB := NewMongoDbLocal(MONGOCONNECTION)
	DB.GetDb().C("portfolio").DropCollection()

	DBremote := NewMongoDbRemote(MONGOCONNECTIONREMOTE)

	DB.UpdatePortfolioAtTime(100, DBremote)

	num, err := DB.GetDb().C("portfolio").Count()
	if err != nil {
		t.Errorf("problems accesing the database")
	}
	if num == 0 {
		t.Errorf("Poblems inserting data into portfolio collection")
	}
}

// func TestRemakePortfolioDb(t *testing.T) {

// }

func TestGetPortfolioMostRecent(t *testing.T) {
	fmt.Println("--Testing GetPortfolioMostRecent--")
	DB := NewMongoDbLocal(MONGOCONNECTION)
	DBremote := NewMongoDbRemote(MONGOCONNECTIONREMOTE)
	DB.GetDb().C("portfolio").DropCollection()

	//Case 1 nothing in the database

	timestamp := DB.GetPortfolioMostRecent()
	if timestamp != 0 {
		t.Errorf("When nothing is in the database should return 0")
	}

	//Case 2 something in the database

	expectedtime := 1000
	DB.UpdatePortfolioAtTime(expectedtime, DBremote)

	timestamp = DB.GetPortfolioMostRecent()

	if expectedtime != timestamp {
		t.Errorf("Issues grabbing the most recent timestep")
	}

	//case 3 multile things in the database

	expectedtime = 2000
	earliertime := 1000

	DB.UpdatePortfolioAtTime(expectedtime, DBremote)
	DB.UpdatePortfolioAtTime(earliertime, DBremote)

	timestamp = DB.GetPortfolioMostRecent()

	if expectedtime != timestamp {
		t.Errorf("Issues grabbing the most recent timestep")
	}

}

func TestGetPortfolioEarliest(t *testing.T) {
	fmt.Println("--Testing GetPortfolioMostRecent--")
	DB := NewMongoDbLocal(MONGOCONNECTION)
	DBremote := NewMongoDbRemote(MONGOCONNECTIONREMOTE)
	DB.GetDb().C("portfolio").DropCollection()

	//Case 1 nothing in the database

	timestamp := DB.GetPortfolioEarliest()
	if timestamp != 0 {
		t.Errorf("When nothing is in the database should return 0")
	}

	//Case 2 something in the database

	expectedtime := 1000
	DB.UpdatePortfolioAtTime(expectedtime, DBremote)

	timestamp = DB.GetPortfolioEarliest()

	if expectedtime != timestamp {
		t.Errorf("Issues grabbing the most recent timestep")
	}

	//case 3 multile things in the database

	expectedtime = 2000
	earliertime := 1000

	DB.UpdatePortfolioAtTime(expectedtime, DBremote)
	DB.UpdatePortfolioAtTime(earliertime, DBremote)

	timestamp = DB.GetPortfolioEarliest()

	if earliertime != timestamp {
		t.Errorf("Issues grabbing the most recent timestep")
	}

}

func TestRemakePortfolioDb(t *testing.T) {
	fmt.Println("--Testing RemakePortfolioDb--")
	DB := NewMongoDbLocal(MONGOCONNECTION)
	DB.RemakePortfoliodDb()

	c_names, _ := DB.GetDb().CollectionNames()
	in_set_flag := false
	for _, elem := range c_names {
		if elem == "portfolio" {
			in_set_flag = true
			break
		}
	}

	if in_set_flag == false {
		t.Errorf("Collection is supposed to exist and it doesn't")
	}

	num, _ := DB.GetDb().C("portfolio").Count()

	if num != 0 {
		t.Errorf("Portfolio Collection Should be Empty")
	}
}

var MAX_DOCUMENT_SIZE_BYTES int = 1000000

func TestCappedCollectionsSameTimeStamp(t *testing.T) {

	DB := NewMongoDbLocal(MONGOCONNECTION)
	// DBremote := NewMongoDbRemote(MONGOCONNECTIONREMOTE)

	var err error

	var coll_info mgo.CollectionInfo
	coll_info.Capped = true
	coll_info.MaxDocs = 4
	coll_info.MaxBytes = MAX_DOCUMENT_SIZE_BYTES

	err = DB.GetDb().C("test").DropCollection()
	if err != nil {
		fmt.Println("error dropping test collection")
	}

	err = DB.GetDb().C("test").Create(&coll_info)
	if err != nil {
		fmt.Println("Problem creating collection")
		fmt.Println(err)
	}

	index := mgo.Index{
		Key:        []string{"timestamp"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	err = DB.GetDb().C("test").EnsureIndex(index)
	if err != nil {
		log.Fatalln(err)
	}

	blah := struct {
		Timestamp int    `json:"timestamp"`
		Content   string `json:"content"`
	}{
		10,
		"blarg",
	}

	err = DB.GetDb().C("test").Insert(blah)
	if err != nil {
		t.Errorf("First document not inserted into capped collection")
	}

	if n, _ := DB.GetDb().C("test").Count(); n == 0 {
		t.Errorf("No documents in the capped collection after first insert")
	}

	err = DB.GetDb().C("test").Insert(blah)
	if err != nil {
		fmt.Println(err)
	}

	if n, _ := DB.GetDb().C("test").Count(); n != 1 {
		t.Errorf("Error with documents being inserted into capped collection of buffsize 1")
	}

}

func TestCappedCollectionsDifferentTimeStamp(t *testing.T) {

	DB := NewMongoDbLocal(MONGOCONNECTION)
	// DBremote := NewMongoDbRemote(MONGOCONNECTIONREMOTE)

	var err error

	var coll_info mgo.CollectionInfo
	coll_info.Capped = true
	coll_info.MaxDocs = 4
	coll_info.MaxBytes = MAX_DOCUMENT_SIZE_BYTES

	err = DB.GetDb().C("test").DropCollection()
	if err != nil {
		fmt.Println("error dropping test collection")
	}

	err = DB.GetDb().C("test").Create(&coll_info)
	if err != nil {
		fmt.Println("Problem creating collection")
		fmt.Println(err)
	}

	index := mgo.Index{
		Key:        []string{"timestamp"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	err = DB.GetDb().C("test").EnsureIndex(index)
	if err != nil {
		log.Fatalln(err)
	}

	blah := struct {
		Timestamp int    `json:"timestamp"`
		Content   string `json:"content"`
	}{
		10,
		"blarg",
	}

	blah2 := struct {
		Timestamp int    `json:"timestamp"`
		Content   string `json:"content"`
	}{
		15,
		"blarg",
	}

	err = DB.GetDb().C("test").Insert(blah)
	if err != nil {
		t.Errorf("First document not inserted into capped collection")
	}

	if n, _ := DB.GetDb().C("test").Count(); n == 0 {
		t.Errorf("No documents in the capped collection after first insert")
	}

	err = DB.GetDb().C("test").Insert(blah2)
	if err != nil {
		fmt.Println(err)
		t.Errorf("Second document not inserted into capped collection")
	}

	if n, _ := DB.GetDb().C("test").Count(); n != 2 {
		t.Errorf("Error with documents being inserted into capped collection of buffsize 1")
	}

}
