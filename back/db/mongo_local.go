package db

import (
	"fmt"
	"log"
	"projects/ridgemont/back/helpers/arrayhelpers"
	"projects/ridgemont/back/helpers/cchelpers"
	"projects/ridgemont/back/models"
	"reflect"

	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const LOCALCCDATABASENAME = "cc_database"

// type DB interface {
// 	SqlDb
// }

type MongoDbLocal struct {
	db *mgo.Database
}

func NewMongoDbLocal(server string) *MongoDbLocal {
	db_ := MongoDbLocal{}
	db_.ConnectDb(server)
	return &db_
}

func (dbl *MongoDbLocal) ConnectDb(server string) {
	//Connect to our local mongo
	s, err := mgo.Dial(server)

	// Check if connection error, is mongo running ?
	if err != nil {
		if DEBUG == true {
			fmt.Println("Trouble connecting to local mongodb server")
		}
		log.Fatalln(err)
		panic(err)
	}

	if err = s.Ping(); err != nil {
		if DEBUG == true {
			fmt.Println("Trouble pining the local mongodb server")
		}
		log.Fatalln(err)
		panic(err)
	}

	fmt.Println("You connected to your local mongo database.")

	DB := s.DB(LOCALCCDATABASENAME)

	dbl.db = DB
}

func (dbl MongoDbLocal) GetDb() *mgo.Database {
	return dbl.db
}

func (dbl MongoDbLocal) Create(m models.Model) error {
	err := dbl.db.C(m.GetTableName()).Insert(m)
	return err
}

func (dbl MongoDbLocal) Delete(m models.Model) error {
	err := dbl.db.C(m.GetTableName()).RemoveId(m.GetId())
	return err
}

func (dbl MongoDbLocal) Index(m models.Model, tablename string) (interface{}, error) {

	modeltype := reflect.PtrTo(reflect.TypeOf(m))
	slice_type := reflect.SliceOf(modeltype)
	// fmt.Println(slice_type)

	a := reflect.New(slice_type).Interface()
	// fmt.Println(a)

	err := dbl.db.C(tablename).Find(bson.M{}).All(a)

	return a, err
}

func (dbl MongoDbLocal) CreatePortfolio() error {

	var err error

	var coll_info mgo.CollectionInfo
	coll_info.Capped = false

	// fmt.Println(coll_info.Capped)

	err = dbl.GetDb().C("portfolio").Create(&coll_info)
	if err != nil {
		fmt.Println("Problem creating collection")
		// log.Fatalln(err)
		return err
	}

	index := mgo.Index{
		Key:        []string{"timestamp"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	err = dbl.GetDb().C("portfolio").EnsureIndex(index)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Collection Created")
	return err
}

func (dbl MongoDbLocal) UpdatePortfolioAtTime(time_ int, dblremote *MongoDbRemote) {

	var val_us, val_btc float64
	var val_us_fiat, val_btc_fiat float64
	var err error

	u := dbl.getTransactionValuesAtTime(time_)
	fiat := dbl.getFiatTransactionValuesAtTime(time_)

	coins := cchelpers.GetCoinSlice(u)

	if len(coins) > 0 {

		b := dblremote.getMarketValuesAtTime(time_, coins)

		val_us, val_btc, err = cchelpers.GetProfileValues(u, b)

		if err != nil {
			log.Fatalln(err)
		}
	} else {
		val_us = 0
		val_btc = 0
	}

	//This section deals with adding in any fiat currency values
	//fiat is the object that holds the transactions
	if len(fiat) > 0 {

		b := dblremote.GetFiatValuesAtTime(time_)
		btc_usd_value := dblremote.GetValueBTC(time_)

		val_us_fiat, val_btc_fiat = cchelpers.GetPortfolioFiatValues(fiat, b, btc_usd_value)

		if err != nil {
			log.Fatalln(err)
		}
	} else {
		val_us_fiat = 0
		val_btc_fiat = 0
	}

	var sum_us, sum_btc float64
	sum_us = val_us + val_us_fiat
	sum_btc = val_btc + val_btc_fiat

	// time_ += time_inc
	port := models.Portfolio{sum_us, sum_btc, time_}
	err = dbl.GetDb().C("portfolio").Insert(port)
	if err != nil {
		// log.Fatalln(err)
		fmt.Println(err)
	}

	// fmt.Println("array built!", num_queries)
}

func (dbl MongoDbLocal) GetPortfolioMostRecent() int {
	vu := models.Portfolio{}
	dbl.GetDb().C("portfolio").Find(bson.M{}).Sort("-timestamp").Limit(1).One(&vu)

	return vu.Timestamp
}

func (dbl MongoDbLocal) GetPortfolioEarliest() int {
	vu := models.Portfolio{}
	dbl.GetDb().C("portfolio").Find(bson.M{}).Sort("timestamp").Limit(1).One(&vu)

	return vu.Timestamp
}

func (dbl MongoDbLocal) RemakePortfoliodDb() {
	err := dbl.GetDb().C("portfolio").DropCollection()
	if err != nil {
		fmt.Println("collection not deleted")
	}
	fmt.Println("portfolio deleted")
	dbl.CreatePortfolio()

	// dbl.UpdatePortfolioFromTime(starttime, time_inc, dblremote)
}

func (dbl MongoDbLocal) getTransactionValuesAtTime(time_ int) []models.CcTransaction {

	pipeline := []bson.M{
		{"$match": bson.M{
			"timestamp": bson.M{"$lte": time_},
		}},
		{"$sort": bson.M{"timestamp": 1}},
		{"$group": bson.M{
			"_id": "$name",
			"temp_id": bson.M{
				"$last": "$_id",
			},
			"quantity": bson.M{
				"$last": "$quantity",
			},
			"timestamp": bson.M{
				"$last": "$timestamp",
			}}},
		{"$project": bson.M{
			"name":      "$_id",
			"_id":       "$temp_id",
			"quantity":  "$quantity",
			"timestamp": "$timestamp",
		}},
		{"$sort": bson.M{"name": 1}},
	}

	u := []models.CcTransaction{}
	pipe := dbl.GetDb().C("cc_transactions").Pipe(pipeline)
	err := pipe.All(&u)
	if err != nil {
		fmt.Println("Error with transactions pipeline in profile build")
		log.Fatalln(err)
	}
	return u
}

func (dbl MongoDbLocal) UpdateCoinListCache(u CoinListMarshaller) {

	dbl.GetDb().C("coinlist").DropCollection() //TODO: probably not good to drop collection if one
	// wants to track histroy of it and such, better to c
	// cap it
	err := dbl.GetDb().C("coinlist").Insert(u)
	if err != nil {
		fmt.Println("error inserting coin list into local cache")
	}
}

func (dbl MongoDbLocal) getFiatTransactionValuesAtTime(time_ int) []models.FiatTransaction {

	pipeline := []bson.M{
		{"$match": bson.M{
			"timestamp": bson.M{"$lte": time_},
		}},
		{"$sort": bson.M{"timestamp": 1}},
		{"$group": bson.M{
			"_id": "$name",
			"temp_id": bson.M{
				"$last": "$_id",
			},
			"quantity": bson.M{
				"$last": "$quantity",
			},
			"timestamp": bson.M{
				"$last": "$timestamp",
			}}},
		{"$project": bson.M{
			"name":      "$_id",
			"_id":       "$temp_id",
			"quantity":  "$quantity",
			"timestamp": "$timestamp",
		}},
		{"$sort": bson.M{"name": 1}},
	}

	u := []models.FiatTransaction{}
	pipe := dbl.GetDb().C("fiat_transactions").Pipe(pipeline)
	err := pipe.All(&u)
	if err != nil {
		fmt.Println("Error with transactions pipeline in profile build")
		log.Fatalln(err)
	}
	return u
}

func (dbl MongoDbLocal) ScanDatabase() []string {
	var u []string

	u, err := dbl.GetDb().CollectionNames()
	if err != nil {
		fmt.Print("Error getting collection names from the local database")
	}

	//remove the cc_transactions collection from the list
	ind := arrayhelpers.SliceIndex(len(u), func(i int) bool { return u[i] == "cc_transactions" })
	if ind != -1 {
		u = append(u[:ind], u[ind+1:]...)
	}

	ind = arrayhelpers.SliceIndex(len(u), func(i int) bool { return u[i] == "portfolio" })
	if ind != -1 {
		u = append(u[:ind], u[ind+1:]...)
	}

	ind = arrayhelpers.SliceIndex(len(u), func(i int) bool { return u[i] == "fiat" })
	if ind != -1 {
		u = append(u[:ind], u[ind+1:]...)
	}

	ind = arrayhelpers.SliceIndex(len(u), func(i int) bool { return u[i] == "fiat_transactions" })
	if ind != -1 {
		u = append(u[:ind], u[ind+1:]...)
	}
	ind = arrayhelpers.SliceIndex(len(u), func(i int) bool { return u[i] == "coinlist" })
	if ind != -1 {
		u = append(u[:ind], u[ind+1:]...)
	}

	return u
}

func (dbl MongoDbLocal) ScanTransactions() []string {
	u := []string{}

	err := dbl.GetDb().C("cc_transactions").Find(bson.M{}).Distinct("name", &u)
	if err != nil {
		fmt.Print("error scanning transactions")
	}
	return u
}

func (dbl MongoDbLocal) GetFiatCurrencylist() []string {
	return []string{"usd", "aud", "cad", "chf", "cny", "gbp", "inr", "jpy", "krw", "mxn", "php", "rub", "eur"}
}

// /////////QUERY
//    {
//         "$match": {
//             "timestamp": {
//                 "$gt": 1502700719
//             }
//         }
//     },
//     {
//         "$limit": 1
//     },
//     {
//         "$project": {
//             "coins" : {
//                     "id": 1,
//                     "priceusd" :1,
//                     "pricebtc" :1

//             },
//             "timestamp": 1
//         }
//     },
//     {
//         "$unwind": "$coins"
//     },
//     {
//         "$match": {
//             "coins.id": {
//                 "$in": ["bitcoin", "ethereum","zcash"]
//             }
//         }
//     },
//     {
//         "$group": {
//             "_id": "$_id",
//             "coins": {
//                 "$addToSet": "$coins"
//             },
//             "timestamp": {"$last": "$timestamp"}
//         }
//     }

///////

// pipeline = []bson.M{
//  {
//      "$match": bson.M{
//          "timestamp": bson.M{
//              "$gt": 1502700719
//          }
//      }
//  },
//  {
//      "$limit": 1
//  },
//  {
//      "$unwind": "$coins"
//  },
//  {
//      "$match": bson.M{
//          "coins.id": bson.M{
//              "$in": ["bitcoin", "ethereum","zcash"]
//          }
//      }
//  },
//  {
//      "$group": bson.M{
//          "_id": "$_id",
//          "coins": bson.M{
//              "$addToSet": "$coins"
//          },
//          "timestamp": {"$last": "$timestamp"}
//      }
//  }
//  }

//query to get the names and symbols of the coins
// {
// 	"$sort": {"timestamp" : -1}
// },
// {
// 	"$limit": 1
// },
// {
// 	"$project": {
// 		"coins" : {
// 				"id": 1,
// 				"symbol" :1,

// 		},
// 		"timestamp": 1
// 	}
// },
// {
// 	"$unwind": "$coins"
// },
// {
// 	"$group": {
// 		"_id": "$_id",
// 		"coins": {
// 			"$addToSet": "$coins"
// 		},
// 	}
// }
