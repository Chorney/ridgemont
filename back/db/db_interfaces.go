package db

import (
	"projects/ridgemont/back/models"

	mgo "gopkg.in/mgo.v2"
)

type RestfulDb interface {
	GetDb() *mgo.Database
	// Insert(interface model) error
	Delete(models.Model) error
	Index(models.Model, string) (interface{}, error)
	Create(models.Model) error
	UpdatePortfolioAtTime(time_ int, dblremote *MongoDbRemote)
	GetPortfolioMostRecent() int
	RemakePortfoliodDb()
	GetPortfolioEarliest() int
	UpdateCoinListCache(CoinListMarshaller)
	ScanDatabase() []string
	ScanTransactions() []string
	GetFiatCurrencylist() []string
	// Show(interface model) error
}

// type RemoteDb interface {
// 	// GetDb() *mgo.Database
// 	Find(from given parameters)
// }
