package db

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

type SqlDbLocal struct {
	db *sql.DB
}

func NewSqlDbLocal(server string) *SqlDbLocal {
	db_ := SqlDbLocal{}
	db_.ConnectDb(server)
	return &db_
}

func (sqldb *SqlDbLocal) ConnectDb(server string) {

	DB, err := sql.Open("postgres", "postgres://postgres@db-users:5432?sslmode=disable")

	if err != nil {
		log.Fatalln(err)
	}

	if err = DB.Ping(); err != nil {
		log.Fatalln(err)
	}
	fmt.Println("You connected to your local POSTGRESS database")

	sqldb.db = DB
}

func (sqldb SqlDbLocal) GetDb() *sql.DB {
	return sqldb.db
}
