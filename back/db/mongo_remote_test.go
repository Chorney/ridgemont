package db

import (
	"fmt"
	"testing"
)

const MONGOCONNECTIONREMOTE = "mongodb://uk_cc_client:rrwrF9YGfBSCfh839@192.168.1.52:27018/cc_databasev2"

func TestRemoteDataBaseConnection(t *testing.T) {

	DB := NewMongoDbRemote(MONGOCONNECTIONREMOTE)

	_, err := DB.GetDb().CollectionNames()

	if err != nil {
		t.Errorf("Issues connecting to the remote database")
	}
}

func TestGetValueBTC(t *testing.T) {

	DB := NewMongoDbRemote(MONGOCONNECTIONREMOTE)

	val := DB.GetValueBTC(0)
	fmt.Println(val)

	val = DB.GetValueBTC(1503710510)
	fmt.Println(val)

	val = DB.GetValueBTC(1503779799)
	fmt.Println(val)

	val = DB.GetValueBTCNow()
	fmt.Println(val)
}
