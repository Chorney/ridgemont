package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/helpers"
	"projects/ridgemont/back/models"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2/bson"
)

type VisualizeController struct {
	Config *config.Config
	tpl    *template.Template
}

type PortfolioCreateMarshaller struct {
	Starttime  int `json:"starttime"`
	Resolution int `json:"resolution"`
}

//TODO: tempates/cc doesn't make sense for this controller, but no templates needes a
func NewVisualizeController(config *config.Config) *VisualizeController {
	tpl := template.Must(template.ParseGlob(os.Getenv("CCPATH") + "/templates/visualize/*"))
	return &VisualizeController{config, tpl}
}

func (vc VisualizeController) GetConfig() *config.Config {
	return vc.Config
}

func (vc VisualizeController) NewIndex(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	if config.Options.TemplateMode == "true" {
		var u models.CcTransaction
		vc.tpl.ExecuteTemplate(w, "new.gohtml", u)
	} else {
		w.WriteHeader(http.StatusForbidden)
		return
	}
}

func (vc VisualizeController) Index(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	var starttime, resolution int
	var rp PortfolioCreateMarshaller

	if config.Options.TemplateMode == "true" {
		starttime_ := req.FormValue("starttime")
		starttime, _ = strconv.Atoi(starttime_)

		resolution_ := req.FormValue("resolution")
		resolution, _ = strconv.Atoi(resolution_)

	} else {
		body, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			errmsg := "visualize, action Index: Error reading request body"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, body)
			return
		}

		err = json.Unmarshal(body, &rp)
		if err != nil {
			errmsg := "visualize, action Index: Error unmarshalling request body from json"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			return
		}
		defer req.Body.Close()

		starttime = rp.Starttime
		resolution = rp.Resolution
	}

	fmt.Println(starttime)
	fmt.Println(resolution)

	cnt, err := vc.Config.MongoDbLocal.GetDb().C("portfolio").Find(bson.M{}).Count()
	if err != nil {
		errmsg := "visualize, action Index: Error connecting to portfolio database, collection may not exist"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusInternalServerError, errmsg, rp)
		return
	}

	p_marshaller := []*models.Portfolio{}
	if err := vc.Config.MongoDbLocal.GetDb().C("portfolio").Find(bson.M{"$and": []bson.M{bson.M{"timestamp": bson.M{"$mod": []int{resolution, 0}}}, bson.M{"timestamp": bson.M{"$gt": starttime}}}}).All(&p_marshaller); err != nil {
		errmsg := "visualize, action Index: Cannot not find requested time series from portfolio, portfolio count: " + strconv.Itoa(cnt)
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
		return
	}

	if config.Options.DebugMode == "true" {
		fmt.Println("visualize, action Index: number of returned data points from portfolio time series:", len(p_marshaller))
	}

	status, user := ccapphelpers.AlreadyLoggedIn(vc.Config, w, req)
	if status == 1 {
		percent := user.PortfolioPercent
		for _, elm := range p_marshaller {
			elm.AlterPortfolio(percent)
		}
	}
	uj, _ := json.Marshal(p_marshaller)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

// Methods have to be capitalized to be exporeted GetUser and not getUser
func (vc VisualizeController) GetCoin(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	u := []models.CcMarketValue{}
	//   coin.Value
	fmt.Println(p[0].Value)
	if err := vc.Config.MongoDbLocal.GetDb().C(p[0].Value).Find(bson.M{}).All(&u); err != nil {
		errmsg := "visualize, action GetCoin: Error Retreiving coin from the ring buffer" + p[0].Value
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	if len(u) == 0 {
		errmsg := "cc_transactions, action Delete: No coins in the ring buffer found"
		if config.Options.DebugMode == "true" {
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, errmsg)
		return
	}

	// Marshal into json
	uj, _ := json.Marshal(u)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)

}
