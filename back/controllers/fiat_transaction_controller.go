package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/helpers"
	"projects/ridgemont/back/helpers/arrayhelpers"
	"projects/ridgemont/back/models"
	"reflect"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2/bson"
)

type FiatCurrencyController struct {
	Config *config.Config
	tpl    *template.Template
}

func (ft FiatCurrencyController) GetConfig() *config.Config {
	return ft.Config
}

func NewFiatCurrencyController(config *config.Config) *FiatCurrencyController {
	tpl := template.Must(template.ParseGlob(os.Getenv("CCPATH") + "/templates/fiat/*"))
	return &FiatCurrencyController{config, tpl}
}

//**// MARHSHALLERS
type FiatTransactionCreateMarshaller struct {
	Quantity  float64 `json:"quantity"`
	Timestamp int     `json:"timestamp"`
	Name      string  `json:"name"`
}

type FiatTransactionDeleteMarshaller struct {
	Id string `json:"id"`
}

//**// END MARHSALLERS

func (ft FiatCurrencyController) New(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	if config.Options.TemplateMode == "true" {
		var u models.FiatTransaction
		ft.tpl.ExecuteTemplate(w, "create_transaction.gohtml", u)
	} else {
		w.WriteHeader(http.StatusForbidden)
		return
	}
}

func (ft FiatCurrencyController) Index(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	// v := models.NewFiatTransactions()
	var v models.FiatTransaction
	vu, err := ft.GetConfig().MongoDbLocal.Index(v, "fiat_transactions")
	if err != nil {
		errmsg := "fiat_transaction, action Index: Error getting transaction data from local database"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, errmsg)
		return
	}

	//vu is an interface returned from the function Index, need to do some type stuff
	val_pointed_to := reflect.Indirect(reflect.ValueOf(vu))
	elems := val_pointed_to.Interface().([]*models.FiatTransaction)

	if config.Options.OauthMode == "false" {
		status, user := ccapphelpers.AlreadyLoggedIn(ft.Config, w, r)
		if status == 1 {
			for _, elm := range elems {
				percent := user.PortfolioPercent
				elm.AlterPortfolio(percent)
			}
		}
	} else {
		status, user, _ := ccapphelpers.AlreadyGoogleLoggedIn(ft.Config, w, r)
		if status == 1 {
			for _, elm := range elems {
				percent := user.PortfolioPercent
				elm.AlterPortfolio(percent)
			}
		}
	}

	if config.Options.TemplateMode == "true" {
		ft.tpl.ExecuteTemplate(w, "transactions.gohtml", elems)
		return
	}

	uj, _ := json.Marshal(elems)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

func (ft FiatCurrencyController) Create(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	var name string
	var timestamp int
	var quantity float64

	var rp TransactionCreateMarshaller

	if config.Options.TemplateMode == "true" {
		name = req.FormValue("name")
		qty := req.FormValue("quantity")
		quantity, _ = strconv.ParseFloat(qty, 64)
		timestamp_ := req.FormValue("timestamp")
		timestamp, _ = strconv.Atoi(timestamp_)

	} else {
		//   queryValues := r.URL.Query()
		//   name = queryValues.Get("c_name")
		//   quantity, _ = strconv.ParseFloat(queryValues.Get("qty"),32)
		//   dtg = queryValues.Get("dtg")

		body, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			errmsg := "fiat_transaction, action Create: Error reading request body"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, body)
			return
		}

		err = json.Unmarshal(body, &rp)
		if err != nil {
			errmsg := "fiat_transaction, action Create: Error unmarshalling request body from json"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			return
		}
		defer req.Body.Close()
		//   un = t.Username
		//   pw = t.Password

		name = rp.Name
		timestamp = rp.Timestamp
		quantity = rp.Quantity

		if config.Options.DebugMode == "true" {
			fmt.Println("Parsed values from the request body")
			fmt.Println("timestamp: ", timestamp)
			fmt.Println("quantity: ", quantity)
			fmt.Println("name: ", name)
		}
	}

	curlist := ft.GetConfig().MongoDbLocal.GetFiatCurrencylist()
	if in, _ := arrayhelpers.InArray(name, curlist); in == true {
		t := models.NewFiatTransaction(name, float64(quantity), timestamp, bson.NewObjectId())
		err := ft.GetConfig().MongoDbLocal.Create(t)

		if err != nil {
			errmsg := "fiat_transactions, action Create: Unable to create new transaction in the local database"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			return
		}

		if config.Options.TemplateMode == "true" {
			http.Redirect(w, req, "/api/fiat/transactions/index", http.StatusSeeOther)
			return
		} else {
			w.WriteHeader(http.StatusCreated) // 200
			return
		}
	} else {
		errmsg := "Selected currency is not in the system: " + name
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
	}

}

func (ft FiatCurrencyController) Delete(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	var id string
	var rp TransactionDeleteMarshaller

	if config.Options.TemplateMode == "true" {
		queryValues := req.URL.Query()
		id = queryValues.Get("id")
	} else {

		body, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			errmsg := "fiat_transaction, action Delete: Error reading request body"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, body)
			return
		}

		err = json.Unmarshal(body, &rp)
		if err != nil {
			errmsg := "fiat_transaction, action Delete: Error unmarshalling request body from json"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			return
		}
		defer req.Body.Close()

		id = rp.Id
		if config.Options.DebugMode == "true" {
			fmt.Println("Id to be deleted", id)
		}
	}

	if !bson.IsObjectIdHex(id) {
		errmsg := "fiat_transaction, action Delete: Id is not bson: " + id
		if config.Options.DebugMode == "true" {
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
		return
	}

	oid := bson.ObjectIdHex(id)
	m := models.NewFiatTransaction("", 0.0, 5, oid)
	err := ft.GetConfig().MongoDbLocal.GetDb().C("fiat_transactions").Find(bson.M{"_id": oid}).One(m)
	name := m.Name
	fmt.Println(name)
	if err != nil {
		errmsg := "fiat_transaction, action Delete: Error finding the collection name of the transaction just selected for deletion: " + name
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
		return
	}

	m = models.NewFiatTransaction("", 0.0, 5, oid)
	err = ft.GetConfig().MongoDbLocal.Delete(m)
	if err != nil {
		errmsg := "fiat_transaction, action Delete: Error deleting transaction from the local dattabase: " + name
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
		return
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/fiat/transactions/index", http.StatusSeeOther)
		return
	} else {
		w.WriteHeader(http.StatusOK) // 200
	}
}

func (ft FiatCurrencyController) GetFiat(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	v := []*models.FiatTransaction{}

	if err := ft.Config.MongoDbLocal.GetDb().C("fiat_transactions").Find(bson.M{"name": p[0].Value}).All(&v); err != nil {
		errmsg := "fiat_transaction, action GetFiat: Currency not found in the fiat transactions database: " + p[0].Value
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	status, user := ccapphelpers.AlreadyLoggedIn(ft.Config, w, r)
	//status is regular user
	if status == 1 {
		percent := user.PortfolioPercent
		for _, elm := range v {
			elm.AlterPortfolio(percent)
		}
	}

	uj, _ := json.Marshal(v)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)

}
