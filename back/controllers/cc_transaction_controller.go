package controllers

import (
	"bytes"
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/helpers"
	"projects/ridgemont/back/helpers/arrayhelpers"
	"projects/ridgemont/back/models"
	"reflect"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2/bson"
)

type CcTransactionController struct {
	Config *config.Config
	tpl    *template.Template
}

func (trc CcTransactionController) GetConfig() *config.Config {
	return trc.Config
}

func NewCcTransactionController(config *config.Config) *CcTransactionController {
	tpl := template.Must(template.ParseGlob(os.Getenv("CCPATH") + "/templates/cc/*"))
	return &CcTransactionController{config, tpl}
}

//**// MARHSHALLERS
type TransactionCreateMarshaller struct {
	Quantity  float64 `json:"quantity"`
	Timestamp int     `json:"timestamp"`
	Name      string  `json:"name"`
}

type TransactionDeleteMarshaller struct {
	Id string `json:"id"`
}

//**// END MARHSALLERS

//For template mode only
func (trc CcTransactionController) New(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	if config.Options.TemplateMode == "true" {
		var u models.CcTransaction
		trc.tpl.ExecuteTemplate(w, "create_transaction.gohtml", u)
	} else {
		w.WriteHeader(http.StatusForbidden)
		return
	}
}

//Get all cc transactions
func (trc CcTransactionController) Index(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	v := models.CcTransaction{}
	vu, err := trc.GetConfig().MongoDbLocal.Index(v, "cc_transactions")
	if err != nil {
		errmsg := "cc_transactions, action Index: Error getting transaction data from local database"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, errmsg)
		return
	}

	//vu is an interface returned from the function Index, need to do some type stuff
	val_pointed_to := reflect.Indirect(reflect.ValueOf(vu))
	elems := val_pointed_to.Interface().([]*models.CcTransaction)

	if config.Options.OauthMode == "false" {
		status, user := ccapphelpers.AlreadyLoggedIn(trc.Config, w, r)
		if status == 1 {
			for _, elm := range elems {
				percent := user.PortfolioPercent
				elm.AlterPortfolio(percent)
			}
		}
	} else {
		status, user, _ := ccapphelpers.AlreadyGoogleLoggedIn(trc.Config, w, r)
		if status == 1 {
			for _, elm := range elems {
				percent := user.PortfolioPercent
				elm.AlterPortfolio(percent)
			}
		}
	}

	if config.Options.TemplateMode == "true" {
		trc.tpl.ExecuteTemplate(w, "transactions.gohtml", elems)
		return
	}

	uj, _ := json.Marshal(elems)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

//Create  transactions, requres POST body with
func (trc CcTransactionController) Create(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	var name string
	var timestamp int
	var quantity float64

	var rp TransactionCreateMarshaller

	if config.Options.TemplateMode == "true" {
		name = req.FormValue("name")
		qty := req.FormValue("quantity")
		quantity, _ = strconv.ParseFloat(qty, 64)
		timestamp_ := req.FormValue("timestamp")
		timestamp, _ = strconv.Atoi(timestamp_)

	} else {
		body, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()

		if err != nil {
			errmsg := "cc_transactions, action Create: Error reading request body"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, body)
			return
		}

		err = json.Unmarshal(body, &rp)
		if err != nil {
			errmsg := "cc_transactions, action Create: Error unmarshalling request body from json"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			return
		}

		name = rp.Name
		timestamp = rp.Timestamp
		quantity = rp.Quantity

		if config.Options.DebugMode == "true" {
			fmt.Println("Parsed values from the request body")
			fmt.Println("timestamp: ", timestamp)
			fmt.Println("quantity: ", quantity)
			fmt.Println("name: ", name)
		}
	}

	ringbuffer_coin_list := trc.GetConfig().MongoDbLocal.ScanDatabase()
	if in, _ := arrayhelpers.InArray(name, ringbuffer_coin_list); in == false {

		values := map[string]string{"name": name}
		jsonValue, _ := json.Marshal(values)
		resp, err := http.Post(trc.GetConfig().Ringbufferaddress+"/api/sys_admin/ringbuffer/createcol", "application/json", bytes.NewBuffer(jsonValue))
		if err != nil {
			errmsg := "cc_transactions, action Create: Error creating new coin in the ring buffer: " + name + " exited no transaction created"

			if config.Options.TemplateMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
				http.Redirect(w, req, "/api/coin/transactions/index", http.StatusSeeOther)
				return
			}
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			if config.Options.ProductionFlag == "true" {
				config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			}
			return
		}

		if resp.StatusCode == 200 {
			fmt.Println("Reponse from ringbuffer for transaction creation OK")
		} else {
			errmsg := "cc_transaction, action Create: Transaction not created, did not receive a 200 response from the ringbuffer: likely bad coin name"
			fmt.Println(errmsg)
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			return
		}
	}

	new_id := bson.NewObjectId()

	t := models.NewCcTransaction(name, quantity, timestamp, new_id)
	err := trc.GetConfig().MongoDbLocal.Create(t)
	response := struct {
		ID bson.ObjectId
	}{new_id}

	if err != nil {
		errmsg := "cc_transactions, action Create: Unable to Create new transaction in the local database"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
		return
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/coin/transactions/index", http.StatusSeeOther)
		return
	} else {
		js, _ := json.Marshal(response)
		w.WriteHeader(http.StatusCreated) // 200
		fmt.Fprint(w, string(js), "\n")
	}

}

func (trc CcTransactionController) Delete(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	var id string
	var rp TransactionDeleteMarshaller

	if config.Options.TemplateMode == "true" {
		queryValues := req.URL.Query()
		id = queryValues.Get("id")
	} else {

		body, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			errmsg := "cc_transaction, action Delete: Error reading request body"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, body)
			return
		}

		err = json.Unmarshal(body, &rp)
		if err != nil {
			errmsg := "cc_transaction, action Delete: Error unmarshalling request body from json"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			return
		}
		defer req.Body.Close()

		id = rp.Id
	}

	if !bson.IsObjectIdHex(id) {
		errmsg := "cc_transaction, action Delete: Id is not bson: " + id
		if config.Options.DebugMode == "true" {
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
		return
	}

	oid := bson.ObjectIdHex(id)
	m := models.NewCcTransaction("", 0.0, 5, oid)
	err := trc.GetConfig().MongoDbLocal.GetDb().C("cc_transactions").Find(bson.M{"_id": oid}).One(m)
	name := m.Name
	if err != nil {
		errmsg := "cc_transactions, action Delete: Error finding the collection name of the transaction just selected for deletion: " + name
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
		return
	}

	m = models.NewCcTransaction("", 0.0, 5, oid)
	err = trc.GetConfig().MongoDbLocal.Delete(m)
	if err != nil {
		errmsg := "cc_transactions, action Delete: Error deleting transaction from the local dattabase: " + name
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
		return
	}

	ringbuffer_coin_list := trc.GetConfig().MongoDbLocal.ScanTransactions()
	if in, _ := arrayhelpers.InArray(name, ringbuffer_coin_list); in == false {

		fmt.Println("sending a message to the ring buffer")

		values := map[string]string{"name": name}
		jsonValue, _ := json.Marshal(values)
		resp, err := http.Post(trc.GetConfig().Ringbufferaddress+"/api/sys_admin/ringbuffer/deletecol", "application/json", bytes.NewBuffer(jsonValue))

		if err != nil {
			errmsg := "cc_transactions, action Delete: Error contacting the ringbuffer: " + name
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorNonEmptyReqPayload(w, http.StatusBadRequest, errmsg, rp)
			return
		}

		if resp.StatusCode == 200 {
			fmt.Println("Transaction deleted succesfully!")
		} else {
			errmsg := "cc_transactions, action Delete: ringbuffer error, transaction has been removed but the ring buffer has not been cleaned up. Remove: " + name + "manually"
			fmt.Println(errmsg)
			config.ResErrorNonEmptyReqPayload(w, http.StatusAccepted, errmsg, rp)
			return
		}
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/coin/transactions/index", http.StatusSeeOther)
		return
	} else {
		w.WriteHeader(http.StatusOK) // 200
	}
}

func (trc CcTransactionController) GetCoin(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	v := []*models.CcTransaction{}

	if err := trc.Config.MongoDbLocal.GetDb().C("cc_transactions").Find(bson.M{"name": p[0].Value}).All(&v); err != nil {
		errmsg := "cc_transactions, action GetCoin: Coin not found in the cc transactions database: " + p[0].Value
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	if config.Options.OauthMode == "false" {
		status, user := ccapphelpers.AlreadyLoggedIn(trc.Config, w, r)
		//status is regular user
		if status == 1 {
			percent := user.PortfolioPercent
			for _, elm := range v {
				elm.AlterPortfolio(percent)
			}
		}
	} else {
		status, user, _ := ccapphelpers.AlreadyGoogleLoggedIn(trc.Config, w, r)
		//status is regular user
		if status == 1 {
			percent := user.PortfolioPercent
			for _, elm := range v {
				elm.AlterPortfolio(percent)
			}
		}
	}

	uj, _ := json.Marshal(v)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)

}
