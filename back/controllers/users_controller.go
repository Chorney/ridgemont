package controllers

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/helpers"
	"projects/ridgemont/back/models"
	"strconv"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
)

type UserController struct {
	config *config.Config
	tpl    *template.Template
}

// var DbUsers map[string]models.User

func NewUserController(config *config.Config) *UserController {
	tpl := template.Must(template.ParseGlob(os.Getenv("CCPATH") + "/templates/user/*"))
	return &UserController{config, tpl}
}

//GET - new user signup page
func (uc UserController) New(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	status, _ := ccapphelpers.AlreadyLoggedIn(uc.config, w, req)
	if status != 0 {
		http.Redirect(w, req, "/api/coin/index", http.StatusSeeOther)
		return
	}

	var u models.User
	uc.tpl.ExecuteTemplate(w, "signup.gohtml", u)
}

//POST - create new user in the DB.
func (uc UserController) Create(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	var s models.Session

	// get form values
	user := models.User{}
	user.UserName = req.FormValue("username")
	pw := req.FormValue("password")
	user.First = req.FormValue("firstname")
	user.Last = req.FormValue("lastname")
	user.Role = req.FormValue("role")
	p_f, err := strconv.ParseFloat(req.FormValue("percent"), 64)
	user.PortfolioPercent = p_f

	// validate form values
	if user.UserName == "" || pw == "" || user.First == "" || user.Last == "" {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	bs, err := bcrypt.GenerateFromPassword([]byte(pw), bcrypt.MinCost)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// insert values
	_, err = uc.config.SqlDb.GetDb().Exec("INSERT INTO users (username, pwhash, first, last, role, p_percent) VALUES ($1, $2, $3, $4, $5, $6)", user.UserName, bs, user.First, user.Last, user.Role, user.PortfolioPercent)
	if err != nil {
		http.Error(w, "Internal server error", http.StatusInternalServerError)
		return
	}

	// create session
	sID := uuid.NewV4()
	c := &http.Cookie{
		Name:  "cc_session",
		Value: sID.String(),
	}
	c.MaxAge = config.SESSIONLENGTH
	if config.Options.TemplateMode == "false" {
		c.Path = "/"
	} else {
		c.Path = "/api/"
	}
	http.SetCookie(w, c)
	s = models.Session{user.UserName, time.Now()}
	uc.config.DbSessions[c.Value] = s

	// redirect
	http.Redirect(w, req, "api/users/index", http.StatusSeeOther)

}

//GET - Show all users in the DB
func (uc UserController) Index(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	if DEBUG == true {
		ccapphelpers.ShowSessions(uc.config)
	}

	rows, err := uc.config.SqlDb.GetDb().Query("SELECT * FROM users;")
	if err != nil {
		fmt.Println("Error - Problem with the query")
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		log.Fatalln(err)
		return
	}
	defer rows.Close()

	users := make([]models.User, 0)
	for rows.Next() {
		user := models.User{}
		fmt.Println(user)
		err := rows.Scan(&user.Id, &user.UserName, &user.Pwhash, &user.First, &user.Last, &user.Role, &user.PortfolioPercent, &user.Created_at, &user.Last_logged_in)
		if err != nil {
			log.Fatalln(err)
			http.Error(w, http.StatusText(500), http.StatusInternalServerError)
			return
		}
		users = append(users, user)
	}
	if err = rows.Err(); err != nil {
		fmt.Println("fuck this")
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}

	uc.tpl.ExecuteTemplate(w, "index.gohtml", users)
}

func (uc UserController) Show(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	user := models.User{}
	un := p[0].Value
	row := uc.config.SqlDb.GetDb().QueryRow("SELECT * FROM users WHERE username = $1", un)

	err := row.Scan(&user.Id, &user.UserName, &user.Pwhash, &user.First, &user.Last, &user.Role, &user.PortfolioPercent, &user.Created_at, &user.Last_logged_in)
	if err != nil {
		fmt.Print("Empty Row")
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}

	uc.tpl.ExecuteTemplate(w, "show.gohtml", user)
}

// DELETE - remove user from the database
func (uc UserController) Destroy(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	if !ccapphelpers.IsAdmin(uc.config, w, req) {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusForbidden)
		return
	}

	un := p[0].Value

	_, err := uc.config.SqlDb.GetDb().Exec("DELETE FROM users WHERE username=$1;", un)
	if err != nil {
		http.Error(w, http.StatusText(500), http.StatusInternalServerError)
		return
	}
}
