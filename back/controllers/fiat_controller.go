package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"net/http"
	"os"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/helpers"
	"projects/ridgemont/back/helpers/cchelpers"
	"projects/ridgemont/back/models"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2/bson"
)

type FiatController struct {
	Config *config.Config
	tpl    *template.Template
}

func NewFiatController(config *config.Config) *FiatController {
	tpl := template.Must(template.ParseGlob(os.Getenv("CCPATH") + "/templates/cc/*"))
	return &FiatController{config, tpl}
}

func (fc FiatController) GetConfig() *config.Config {
	return fc.Config
}

// Methods have to be capitalized to be exporeted GetUser and not getUser
func (fc FiatController) Index(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	//   u := models.CcQuantity{}

	u := []models.FiatTransaction{}
	//   fmt.Print(w,u)

	pipeline := []bson.M{
		{"$sort": bson.M{"timestamp": 1}},
		{"$group": bson.M{
			"_id": "$name",
			"temp_id": bson.M{
				"$last": "$_id",
			},
			"quantity": bson.M{
				"$last": "$quantity",
			},
			"timestamp": bson.M{
				"$last": "$timestamp",
			}}},
		{"$project": bson.M{
			"name":      "$_id",
			"_id":       "$temp_id",
			"quantity":  "$quantity",
			"timestamp": "$timestamp",
		}},
	}

	// }
	pipe := fc.Config.MongoDbLocal.GetDb().C("fiat_transactions").Pipe(pipeline)
	err := pipe.All(&u)

	if err != nil {
		errmsg := "fiat, action Index: Error getting the most recent fiat currency transactions"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, errmsg)
		return
	}

	vu := models.FiatCurrency{}
	err = fc.Config.MongoDbLocal.GetDb().C("fiat").Find(bson.M{}).Sort("-timestamp").Limit(1).One(&vu)

	if err != nil {
		errmsg := "fiat, action Index: Error grabbing the fiat currency values from the ringbuffer cache"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, errmsg)
		return
	}

	btc_val := 1 / vu.Btcusdvalue

	var the_marshaller_elements []cchelpers.FiatSummaryElement
	for _, t := range u {

		var val float64
		var fiat cchelpers.FiatSummaryElement

		switch t.Name {
		case "cad":
			val = 1 / vu.Rates.Cad
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "eur":
			val = 1 / vu.Rates.Eur
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "gbp":
			val = 1 / vu.Rates.Gbp
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "chf":
			val = 1 / vu.Rates.Chf
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "Cny":
			val = 1 / vu.Rates.Cny
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "Inr":
			val = 1 / vu.Rates.Inr
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "jpy":
			val = 1 / vu.Rates.Jpy
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "krw":
			val = 1 / vu.Rates.Krw
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "mxn":
			val = 1 / vu.Rates.Mxn
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "php":
			val = 1 / vu.Rates.Php
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "rub":
			val = 1 / vu.Rates.Rub
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		case "usd":
			val = 1.0
			btc_val = btc_val / val
			fiat = cchelpers.FiatSummaryElement{t.Name, t.Quantity, t.Timestamp, t.ID, val, btc_val}
		default:
			fmt.Println("currency not available in selection", t.Name)
		}
		the_marshaller_elements = append(the_marshaller_elements, fiat)
	}

	//status is regular user
	if config.Options.OauthMode == "false" {
		status, user := ccapphelpers.AlreadyLoggedIn(fc.Config, w, r)
		if status == 1 {
			for _, elm := range the_marshaller_elements {
				elm.Quantity = elm.Quantity * user.PortfolioPercent
			}
		}
	} else {
		status, user, _ := ccapphelpers.AlreadyGoogleLoggedIn(fc.Config, w, r)
		if status == 1 {
			for _, elm := range the_marshaller_elements {
				elm.Quantity = elm.Quantity * user.PortfolioPercent
			}
		}
	}

	uj, _ := json.Marshal(the_marshaller_elements)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)

}

func (fc FiatController) GetAvailableCoins(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	u := []string{"aud", "cad", "chf", "cny", "gbp", "inr", "jpy", "krw", "mxn", "php", "rub", "eur"}

	fmt.Println("number of currencies in the list", len(u))
	v, _ := json.Marshal(u)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", v)
}
