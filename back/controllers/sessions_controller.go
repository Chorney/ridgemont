package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"os"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/helpers"
	"projects/ridgemont/back/models"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/satori/go.uuid"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
)

var (
	googleOauthConfig = &oauth2.Config{
		RedirectURL:  "http://localhost:8080/api/session/googlecallback",
		ClientID:     "262674666157-m6f8reo7ahrnms71oeq79jt0jg64tara.apps.googleusercontent.com", //os.Getenv("googlekey"),
		ClientSecret: "vg0ofrMeT0dNXoplQMcjAVJa",                                                 //os.Getenv("googlesecret"),
		Scopes: []string{"https://www.googleapis.com/auth/userinfo.profile",
			"https://www.googleapis.com/auth/userinfo.email"},
		Endpoint: google.Endpoint,
	}
	// Some random string, random for each request
	oauthStateString = "random"
)

type SessionController struct {
	config *config.Config
	tpl    *template.Template
}

type LoginStruct struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func NewSessionController(config *config.Config) *SessionController {
	tpl := template.Must(template.ParseGlob(os.Getenv("CCPATH") + "/templates/session/*"))
	return &SessionController{config, tpl}
}

func (sc SessionController) GetConfig() *config.Config {
	return sc.config
}

var DEBUG bool = true

//GoogleLogin GET login form
func (sc SessionController) GoogleLoginTemplateMode(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	url := googleOauthConfig.AuthCodeURL(oauthStateString)
	http.Redirect(w, r, url, http.StatusTemporaryRedirect)
}

func (sc SessionController) GoogleCallback(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	state := r.FormValue("state")
	if state != oauthStateString {
		fmt.Printf("invalid oauth state, expected '%s', got '%s'\n", oauthStateString, state)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}

	code := r.FormValue("code")
	token, err := googleOauthConfig.Exchange(oauth2.NoContext, code)
	if err != nil {
		fmt.Println("Code exchange failed with '%s'\n", err)
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	fmt.Fprint(w, token)

	id_token := token.Extra("id_token").(string)

	response, err := http.Get("https://www.googleapis.com/oauth2/v2/userinfo?access_token=" + token.AccessToken)
	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)
	fmt.Fprintf(w, "Content: %s\n", contents)

	response2, err := http.Get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + id_token)
	defer response.Body.Close()
	contents, err = ioutil.ReadAll(response2.Body)
	fmt.Fprintf(w, "Content: %s\n", contents)
}

func (sc SessionController) GoogleLogin(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	body, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		errmsg := "sessions, action GoogleLogin: Error reading request body"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	var t models.TokenMarshaller
	err = json.Unmarshal(body, &t)
	if err != nil {
		errmsg := "sessions, action GoogleLogin: Error unmarshalling request body from json"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	response, err := http.Get("https://www.googleapis.com/oauth2/v3/tokeninfo?id_token=" + t.IdToken)
	if config.Options.DebugMode == "true" {
		fmt.Printf("#v", response)
	}

	defer response.Body.Close()
	contents, err := ioutil.ReadAll(response.Body)

	var idt models.IdTokenMarshaller
	err = json.Unmarshal(contents, &idt)

	if err != nil {
		errmsg := "sessions, action GoogleLogin: Error unmarshalling request body from json"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	if idt.Iss != "accounts.google.com" && idt.Iss != "https://accounts.google.com" {
		errmsg := "sessions, action GoogleLogin: Wrong Issuer, token is not from google"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusForbidden, errmsg)
		return
	}

	if _, ok := sc.config.DbUsers[idt.Sub]; !ok {
		errmsg := "sessions, action GoogleLogin: Not an Accepted User ID"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusForbidden, errmsg)
		return
	}

	val := sc.config.DbUsers[idt.Sub]

	w.WriteHeader(http.StatusCreated)
	uj, _ := json.Marshal(struct {
		Admin int `json: "admin"`
	}{val.Admin})
	fmt.Fprintf(w, "%s\n", uj)
}

func (sc SessionController) GoogleLogout(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	w.WriteHeader(http.StatusNoContent)
}

// Old login stuff -------------------------------------------------------------------

// GET login form
func (sc SessionController) New(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	if config.Options.TemplateMode == "true" {
		status, _ := ccapphelpers.AlreadyLoggedIn(sc.config, w, req)
		if status != 0 {
			http.Redirect(w, req, "/api/summary", http.StatusSeeOther)
			return
		}
		var u models.User
		sc.tpl.ExecuteTemplate(w, "new.gohtml", u)
	} else {
		w.WriteHeader(http.StatusForbidden)
		return
	}
}

//POST login form
func (sc SessionController) Create(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	var un, pw string

	if config.Options.TemplateMode == "true" {
		un = req.FormValue("username")
		pw = req.FormValue("password")
	} else {
		body, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			errmsg := "sessions, action Create: Error reading request body"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
			return
		}

		var t LoginStruct
		err = json.Unmarshal(body, &t)
		if err != nil {
			errmsg := "sessions, action Create: Error unmarshalling request body from json"
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
			return
		}
		defer req.Body.Close()
		un = t.Username
		pw = t.Password
	}

	user := models.User{}
	row := sc.config.SqlDb.GetDb().QueryRow("SELECT * FROM users WHERE username = $1", un)

	err := row.Scan(&user.Id, &user.UserName, &user.Pwhash, &user.First, &user.Last, &user.Role, &user.PortfolioPercent, &user.Created_at, &user.Last_logged_in)

	if err != nil {
		if config.Options.TemplateMode == "true" {
			http.Error(w, "Username and/or password do not match", http.StatusBadRequest)
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		errmsg := "sessions, action Create: Error pulling user data from the database"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusBadRequest, errmsg)
		return
	}

	// does the entered password match the stored password?
	err = bcrypt.CompareHashAndPassword(user.Pwhash, []byte(pw))
	if err != nil {
		if config.Options.TemplateMode == "true" {
			http.Error(w, "Username and/or password do not match", http.StatusUnauthorized)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		errmsg := "sessions, action Create: Username and/or password do not match"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		config.ResErrorEmptyReqPayload(w, http.StatusUnauthorized, errmsg)
		return
	}
	// create session
	sID := uuid.NewV4()
	c := &http.Cookie{
		Name:  "cc_session",
		Value: sID.String(),
	}
	c.MaxAge = config.SESSIONLENGTH
	if config.Options.TemplateMode == "false" {
		c.Path = "/"
	} else {
		c.Path = "/api/"
	}
	fmt.Println(c.MaxAge)
	http.SetCookie(w, c)
	sc.config.DbSessions[c.Value] = models.Session{user.UserName, time.Now()}
	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/users/index", http.StatusSeeOther)
		return
	} else {
		w.WriteHeader(http.StatusCreated)
		uj, _ := json.Marshal(struct {
			Role string `json: "role"`
		}{user.Role})
		fmt.Fprintf(w, "%s\n", uj)
	}
}

//
// //Logout
func (sc SessionController) Destroy(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
	status, _ := ccapphelpers.AlreadyLoggedIn(sc.config, w, req)
	if status == 0 {
		if config.Options.TemplateMode == "true" {
			http.Redirect(w, req, "/api/coin/transactions/index", http.StatusSeeOther)
		} else {
			w.WriteHeader(http.StatusUnauthorized)
		}
		return
	}
	c, _ := req.Cookie("cc_session")
	// delete the session
	delete(sc.config.DbSessions, c.Value)
	// remove the cookie
	c = &http.Cookie{
		Name:   "cc_session",
		Value:  "",
		MaxAge: -1,
	}
	http.SetCookie(w, c)

	// clean up dbSessions
	if time.Now().Sub(sc.config.DbSessionsCleaned) > (time.Second * 30) {
		go ccapphelpers.CleanSessions(sc.config)
	}

	if config.Options.TemplateMode == "true" {
		http.Redirect(w, req, "/api/session/login", http.StatusSeeOther)
	} else {
		w.WriteHeader(http.StatusOK)
	}
}
