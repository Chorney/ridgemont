package controllers

import (
	"bytes"
	"encoding/json"
	"flag"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"projects/ridgemont/back/config"
	"testing"

	"gopkg.in/mgo.v2/bson"
)

// type Config struct {
// 	SqlDb             *db.SqlDbLocal
// 	MongoDbLocal      db.RestfulDb
// 	MongoDbRemote     *db.MongoDbRemote
// 	DbSessions        map[string]models.Session
// 	DbSessionsCleaned time.Time
// }

// var MongoDbLocal db.RestfulDb
// var MongoDbRemote *db.MongoDbRemote
// var MongoDbLocal *SqlDbLocal

// func createmockconfig () *config.Config {
// 	cfg := config.Config{SqlDb,MongoDbLocal,MongoDbRemote}

// }

// func MockHandle(w http.ResponseWriter, *http.Request, p [])

func processFlags() {

	flag.StringVar(&config.Options.ProductionFlag, "production", "false", "When in production mode uses other config file variables (default false)")
	flag.StringVar(&config.Options.TemplateMode, "tplmode", "true", "Template mode means you can only log in by sending a post, and a number of other options")
	flag.StringVar(&config.Options.UnlockedMode, "unlockedmode", "false", "If unlocked mode == true than all pages are open, and no need to login, default is false")
	flag.StringVar(&config.Options.DebugMode, "debugmode", "true", "When debug mode is true, errors and messages are printed to the terminal (default true)")
	flag.Parse()
}

type TransactionCheckMarshaller struct {
	Quantity  float64 `json:"quantity"`
	Timestamp int     `json:"timestamp"`
	Name      string  `json:"name"`
}

func TestCcTransactionCreate(t *testing.T) {

	processFlags()

	config.Options.TemplateMode = "false"
	cfg := config.NewConfig()
	trc := NewCcTransactionController(cfg)

	//create body for request

	var Ids []bson.ObjectId

	tt := []struct {
		Data struct {
			Name      string  `json:"name"`
			Quantity  float64 `json:"quantity"`
			Timestamp int     `json:"timestamp"`
		}
		StatusCode int
	}{
		{struct {
			Name      string  `json:"name"`
			Quantity  float64 `json:"quantity"`
			Timestamp int     `json:"timestamp"`
		}{"neo", 123.233, 123123}, http.StatusCreated},
		{struct {
			Name      string  `json:"name"`
			Quantity  float64 `json:"quantity"`
			Timestamp int     `json:"timestamp"`
		}{"tacolibre", 123.233, 123123}, http.StatusBadRequest},
	}

	for _, tc := range tt {
		m, _ := json.Marshal(tc.Data)

		req, err := http.NewRequest("POST", "localhost:8080/api/coin/index", bytes.NewBuffer(m))
		if err != nil {
			t.Fatalf("Could not create created request: %v", err)

		}
		rec := httptest.NewRecorder()

		trc.Create(rec, req, nil)

		res := rec.Result()
		defer res.Body.Close()
		if res.StatusCode != tc.StatusCode {
			t.Errorf("expected satus OK; go %v", res.StatusCode)
		}

		if res.StatusCode == http.StatusCreated {
			body, err := ioutil.ReadAll(res.Body)
			if err != nil {
				t.Errorf("error reading response body")
			}
			rp := struct{ ID bson.ObjectId }{}
			err = json.Unmarshal(body, &rp)
			if err != nil {
				t.Errorf("issues unmarsalling response from create handler")
			}
			Ids = append(Ids, rp.ID)
		}

	}

	//delete the ids just created, which will test the delete function
	for _, id_ := range Ids {
		// fmt.Println(id_.Hex())
		msg := struct {
			Id string `json: "id"`
		}{id_.Hex()}
		m, _ := json.Marshal(msg)
		req, err := http.NewRequest("DELETE", "localhost:8080/api/coin/delete", bytes.NewBuffer(m))
		if err != nil {
			t.Fatalf("Could not create created request: %v", err)

		}
		rec := httptest.NewRecorder()

		trc.Delete(rec, req, nil)

		res := rec.Result()
		defer res.Body.Close()
		if res.StatusCode != http.StatusOK {
			t.Errorf("expected satus OK; go %v", res.StatusCode)
		}
	}

	random_id := bson.NewObjectId()
	msg := struct {
		Id string `json: "id"`
	}{random_id.Hex()}
	m, _ := json.Marshal(msg)
	req, err := http.NewRequest("DELETE", "localhost:8080/api/coin/delete", bytes.NewBuffer(m))
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)

	}
	rec := httptest.NewRecorder()

	trc.Delete(rec, req, nil)

	res := rec.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusBadRequest {
		t.Errorf("expected status BadRequest; got %v", res.StatusCode)
	}

}

func TestCcTransactionIndex(t *testing.T) {

	// processFlags()

	// config.Options.TemplateMode = "false"

	cfg := config.NewConfig()
	trc := NewCcTransactionController(cfg)

	req, err := http.NewRequest("GET", "localhost:8080/api/coin/index", nil)
	if err != nil {
		t.Fatalf("Could not create created request: %v", err)

	}
	rec := httptest.NewRecorder()

	trc.Index(rec, req, nil)

	res := rec.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("expected satus OK; go %v", res.StatusCode)
	}

	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Fatalf("could not read response")
	}

	rp := []TransactionCheckMarshaller{}
	err = json.Unmarshal(b, &rp)
	if err != nil {
		t.Errorf("trouble unmarshalling response into json")
	}

	if len(rp) == 0 {
		t.Errorf("List of transactions should be non zero (if the database was previously seeded)")
	}
}
