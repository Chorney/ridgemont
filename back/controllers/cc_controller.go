package controllers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
	"projects/ridgemont/back/config"
	"projects/ridgemont/back/helpers"
	"projects/ridgemont/back/helpers/cchelpers"
	"projects/ridgemont/back/models"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2/bson"
)

type RestfulCtrl interface {
	Ctrl
	Index(w http.ResponseWriter, req *http.Request, p httprouter.Params)
	New(w http.ResponseWriter, req *http.Request, p httprouter.Params)
	Create(w http.ResponseWriter, req *http.Request, p httprouter.Params)
	Delete(w http.ResponseWriter, req *http.Request, p httprouter.Params)
}

type Ctrl interface {
	GetConfig() *config.Config
}

type Marshaller interface {
	AlterQuantity(float64)
}

type CcController struct {
	Config *config.Config
	tpl    *template.Template
}

type CoinMarshaller struct {
	Transactions []models.CcTransaction `json:"transactions"`
	MarketValue  []models.CcMarketValue `json:"marketvalue"`
}

func NewCcController(config *config.Config) *CcController {
	tpl := template.Must(template.ParseGlob(os.Getenv("CCPATH") + "/templates/cc/*"))
	return &CcController{config, tpl}
}

func (cc_c CcController) GetConfig() *config.Config {
	return cc_c.Config
}

// Methods have to be capitalized to be exporeted GetUser and not getUser
func (cc_c CcController) Index(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	//   u := models.CcQuantity{}

	u := []models.CcTransaction{}
	//   fmt.Print(w,u)

	pipeline := []bson.M{
		{"$sort": bson.M{"timestamp": 1}},
		{"$group": bson.M{
			"_id": "$name",
			"temp_id": bson.M{
				"$last": "$_id",
			},
			"quantity": bson.M{
				"$last": "$quantity",
			},
			"timestamp": bson.M{
				"$last": "$timestamp",
			}}},
		{"$project": bson.M{
			"name":      "$_id",
			"_id":       "$temp_id",
			"quantity":  "$quantity",
			"timestamp": "$timestamp",
		}},
	}

	pipe := cc_c.Config.MongoDbLocal.GetDb().C("cc_transactions").Pipe(pipeline)
	err := pipe.All(&u)

	if err != nil {
		errmsg := "cc, action Index: Error retrieving cc transaction data"
		if config.Options.DebugMode == "true" {
			log.Fatalln(err)
			fmt.Println(errmsg)
		}
		if config.Options.ProductionFlag == "true" {
			config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, errmsg)
			return
		}
	}

	var the_marshaller_elements []*cchelpers.CcSummaryElement
	for _, t := range u {

		name := t.Name

		vu := models.CcMarketValue{}
		if err := cc_c.Config.MongoDbLocal.GetDb().C(name).Find(bson.M{}).Sort("-lastupdated").Limit(1).One(&vu); err != nil {
			errmsg := "cc, action Index: Could not find particular coin in the ring buffer: " + name
			if config.Options.DebugMode == "true" {
				fmt.Println(err)
				fmt.Println(errmsg)
			}
			if config.Options.ProductionFlag == "true" {
				config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, errmsg)
				return
			}

		} else {
			new_ := cchelpers.JoinTrsCoin(t, vu)
			the_marshaller_elements = append(the_marshaller_elements, &new_)
		}
	}

	if config.Options.OauthMode == "false" {
		status, user := ccapphelpers.AlreadyLoggedIn(cc_c.Config, w, r)
		//status is regular user
		if status == 1 {
			for _, elm := range the_marshaller_elements {
				elm.SetQuantity(user.PortfolioPercent)
			}
		}
	} else {
		status, user, _ := ccapphelpers.AlreadyGoogleLoggedIn(cc_c.Config, w, r)
		//status is regular user

		if status == 1 {
			for _, elm := range the_marshaller_elements {
				elm.SetQuantity(user.PortfolioPercent)
			}
		}
	}

	uj, _ := json.Marshal(the_marshaller_elements)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", uj)
}

type CoinListMarshaller struct {
	ID    string `json:"_id"`
	Coins []struct {
		Name   string `json:"name" bson:"id"`
		Symbol string `json:"symbol"`
		Rank   string `json:"rank"`
	} `json:"coins"`
}

func (cc_c CcController) GetAvailableCoins(w http.ResponseWriter, r *http.Request, p httprouter.Params) {

	u := CoinListMarshaller{}

	err := cc_c.Config.MongoDbLocal.GetDb().C("coinlist").Find(bson.M{}).One(&u)
	if err != nil {
		errmsg := "cc, action GetAvailableCoins: Error finding coins from the local coin list cache"
		if config.Options.DebugMode == "true" {
			fmt.Println(err)
			fmt.Println(errmsg)
		}
		if config.Options.ProductionFlag == "true" {
			config.ResErrorEmptyReqPayload(w, http.StatusInternalServerError, errmsg)
			return
		}

	}

	fmt.Println("cc, action GetAvailableCoins: number of coins in the list", len(u.Coins))
	v, _ := json.Marshal(u.Coins)
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "%s\n", v)
}
