# Crypto Portfolio app
A golang backend application for cryptocurrency portfolio. Larger application to include a React UI. 

## Index
* back: the main golang webserver
* db_coins: the mongodb for coins
* db_users: postgresdb for users
* jump: for connecting to a remote server to pull data into ringbuffer
* rebuild: app for rebuilding portfolios vs. time in the mongodb
* ringbuffer: capped mongodb for holding most recent portfolio data

