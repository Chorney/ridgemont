--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.4
-- Dumped by pg_dump version 9.6.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer NOT NULL,
    username text NOT NULL,
    pwhash text NOT NULL,
    first text NOT NULL,
    last text NOT NULL,
    role text NOT NULL,
    p_percent real NOT NULL,
    last_logged_in timestamp without time zone DEFAULT now(),
    created_at timestamp without time zone DEFAULT now(),
    CONSTRAINT check_role_good CHECK ((role = ANY (ARRAY['admin'::text, 'user'::text, 'manager'::text])))
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, username, pwhash, first, last, role, p_percent, last_logged_in, created_at) FROM stdin;
13	user@user.com	$2a$04$R4viyyp9jYJGFTqXJ5FCgeQLgDLZJlKk5BHbvIr/tkAcGOZBv8Nje	user	user	user	0.0500000007	2017-09-02 04:38:34.45012	2017-09-02 04:38:34.45012
14	manager@manager.com	$2a$04$2LbQINZknM3zTHaDVm2rKOGM95pcrxyBfId9KHzLveJ7I4DkqM5oq	manager	manager	manager	0.150000006	2017-09-02 06:12:31.760378	2017-09-02 06:12:31.760378
15	admin@admin.com	$2a$04$zKir7ws.tIjc5Q7B3u/haO6JvCl7V6/W/CHItyxRCk.qWhKYeQabO	admin	admin	admin	112	2017-09-02 07:15:43.24465	2017-09-02 07:15:43.24465
16	blargo@hotmail.com	$2a$04$N7OIWjWUPk8Mu/ebWMoAXu1I5IqJEwfGp5mxVkMHxkekNQ5Zx2X.S	blargo	smith	user	10	2017-09-02 07:32:34.607265	2017-09-02 07:32:34.607265
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 17, true);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: users users_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_username_key UNIQUE (username);


--
-- PostgreSQL database dump complete
--

