# This database holds the users for the cc app
1. The postgres data volume must be mounted to /var/lib/postgresql/data and ports mapped (5432:5432). For example:
`docker run -v /db-users/data:/var/lib/postgresql/data -p 5432:5432 postgres`

2. The /data directory is already populated with two users.
