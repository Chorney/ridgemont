package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"projects/ridgemont/back/db"
	"time"

	"github.com/julienschmidt/httprouter"
)

type Config struct {
	Localconn  string `json:"localconn"`
	Remoteconn string `json:"remoteconn"`
}

type FlagConfig struct {
	ProductionFlag string
	TemplateMode   string
}

type Application struct {
	config Config
}

var MongoDbLocal db.RestfulDb
var MongoDbRemote *db.MongoDbRemote

var Options FlagConfig

func parseConfig() Config {
	var err error
	var raw []byte
	if Options.ProductionFlag == "true" {
		raw, err = ioutil.ReadFile("/opt/config.json")
	} else {
		raw, err = ioutil.ReadFile("config.json")
	}
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	c := Config{}
	json.Unmarshal(raw, &c)
	return c
}

func main() {

	processFlags()

	if Options.TemplateMode == "true" {
		fmt.Println("running in Template Mode")
	}

	if Options.ProductionFlag == "true" {
		fmt.Println("running in Production mode")
	}

	config := parseConfig()

	MongoDbLocal = db.NewMongoDbLocal(config.Localconn)
	MongoDbRemote = db.NewMongoDbRemote(config.Remoteconn)

	r := httprouter.New()

	c_suspend := make(chan int)
	c_resume := make(chan int)
	go ContinousPortfolioBuilding(300, c_suspend, c_resume)

	//**// cryptocurrency controller stuff
	r.GET("/api/sys_admin/rebuildportfolio", MakeHandler(RebuildPortfolio, c_suspend, c_resume))
	if Options.ProductionFlag == "false" {
		http.ListenAndServe("localhost:8081", r)
	} else {
		http.ListenAndServe(":8081", r)
	}

}

func processFlags() {

	flag.StringVar(&Options.ProductionFlag, "production", "false", "When in production mode uses other config file variables (default false)")
	flag.StringVar(&Options.TemplateMode, "tplmode", "true", "Template mode means you can only log in by sending a post, and a number of other options")
	flag.Parse()
}

func MakeHandler(fn func(w http.ResponseWriter, r *http.Request, p httprouter.Params), c_suspend chan int, c_resume chan int) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		// w.Header() .Set("Access-Control-Allow-Origin","*")
		if Options.TemplateMode == "false" {
			w.Header().Set("Content-Type", "application/json")
		}
		num := 1
		fmt.Println("waiting for pass signal")
		c_suspend <- num
		fn(w, r, p)
		c_resume <- num
	}
}

// temp doc used for creating the empty collections

func RebuildPortfolio(w http.ResponseWriter, req *http.Request, p httprouter.Params) {

	MongoDbLocal.RemakePortfoliodDb() //drops the collection

}

func ContinousPortfolioBuilding(time_inc int, c_suspend chan int, c_resume chan int) {
	var lasttime int
	for true {
		lasttime = MongoDbLocal.GetPortfolioMostRecent()
		newtime := lasttime + time_inc
		lastest_remote_time := MongoDbRemote.GetTimeLatest()
		select {
		case <-c_suspend: //locked because the portfolio is being reimaged,
			<-c_resume
		default:
			if lasttime == 0 { //nothing is in the database, right now just set the beginning point as the first element in the remote db
				earliest_remote_time := MongoDbRemote.GetTimeEarliest()
				date_init := time.Unix(int64(earliest_remote_time), 0)
				time_init := time.Date(date_init.Year(), date_init.Month(), date_init.Day(), date_init.Hour(), 0, 0, 0, date_init.Location())
				time_ := int(time_init.Unix())
				MongoDbLocal.UpdatePortfolioAtTime(time_, MongoDbRemote)
			} else if newtime > lastest_remote_time {
				fmt.Println("Sleep Case 2")
				time.Sleep(time.Duration(1) * time.Minute) // 1 minute time resolution just in case need to catch a database refresh
			} else {
				MongoDbLocal.UpdatePortfolioAtTime(newtime, MongoDbRemote)
			}
		}

	}
}
