package main

import (
	"fmt"
	"projects/ridgemont/back/db"
	"testing"

	"gopkg.in/mgo.v2/bson"
)

func TestParseConfig(t *testing.T) {

	processFlags()
	config := parseConfig()

	expectedMongoDbLocalConn := "mongodb://db-coins"
	expectedMongoDbRemoteConn := "mongodb://uk_cc_client:rrwrF9YGfBSCfh839@192.168.1.52:27018/cc_databasev2"

	if config.Localconn != expectedMongoDbLocalConn {
		fmt.Println(config.Localconn)
		t.Errorf("Issues parsing the config file")
	}

	if config.Remoteconn != expectedMongoDbRemoteConn {
		fmt.Println(config.Remoteconn)
		t.Errorf("Issues parsing the config file")
	}

}

func TestUpdateFiatCache(t *testing.T) {

	// processFlags()
	// config := parseConfig()

	MongoDbLocal = db.NewMongoDbLocal("mongodb://db-coins")
	MongoDbRemote = db.NewMongoDbRemote("mongodb://uk_cc_client:rrwrF9YGfBSCfh839@192.168.1.52:27018/cc_databasev2")

	fiatobject := MongoDbRemote.GetFiatMostRecentValue()
	btcusdvalue := MongoDbRemote.GetValueBTCNow()

	fiatobject.Btcusdvalue = btcusdvalue

	_, err := MongoDbLocal.GetDb().C("fiat").RemoveAll(bson.M{})
	if err != nil {
		fmt.Println(err)
	}
	// fmt.Println(btcusdvalue)
	// fmt.Println(fiatobject.Btcusdvalue)
	// MongoDbLocal.GetDb().C("fiat").Insert(fiatobject)

	// vu := models.FiatCurrency{}
	// err := MongoDbLocal.GetDb().C("fiat").Find(bson.M{}).Sort("-timestamp").Limit(1).One(&vu)
	// if err != nil {
	// 	t.Errorf("Problems accessing database")
	// }

	// if btcusdvalue != vu.Btcusdvalue {
	// 	t.Errorf("inserted btc value does not match value retrived from the database")
	// }

}
