package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"projects/ridgemont/back/db"
	"projects/ridgemont/back/helpers/arrayhelpers"
	"projects/ridgemont/back/models"
	"time"

	"github.com/julienschmidt/httprouter"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

var MAX_BUFF_SIZE int = 288

const MAX_FIAT_BUFF_SIZE int = 2 //buffsize needs to be 2, if there is only 1 spot, there are issues with the duplicates and then the capped collections, need 1 spot as a buffer to the one spot wanted as a cache.

var MAX_DOCUMENT_SIZE_BYTES int = 1000000
var DEBUG bool = true

var PRODUCTION bool = true

type Config struct {
	Localconn  string `json:"localconn"`
	Remoteconn string `json:"remoteconn"`
}

type Application struct {
	config Config
}

type FlagConfig struct {
	ProductionFlag string
	TemplateMode   string
}

type CollectionCreateMarshaller struct {
	Name string `json:"name"`
}

var Options FlagConfig

var MongoDbLocal db.RestfulDb
var MongoDbRemote *db.MongoDbRemote

func main() {

	app := NewApplication()

	app.InitService()

	r := httprouter.New()

	r.GET("/api/sys_admin/ringbuffer/reset", MakeResetHandler(app))
	r.GET("/api/sys_admin/ringbuffer/update", MakeUpdateColsHandler(app))
	r.POST("/api/sys_admin/ringbuffer/createcol", MakeCreateColHandler(app))
	r.POST("/api/sys_admin/ringbuffer/deletecol", MakeDeleteColHandler(app))

	if Options.ProductionFlag == "false" {
		http.ListenAndServe("localhost:8082", r)
	} else {
		http.ListenAndServe(":8082", r)
	}

}

// temp doc used for creating the empty collections

func NewApplication() *Application {

	processFlags()

	if Options.TemplateMode == "true" {
		fmt.Println("running in Template Mode")
	}

	if Options.ProductionFlag == "true" {
		fmt.Println("running in Production mode")
	}

	config := parseConfig()

	MongoDbLocal = db.NewMongoDbLocal(config.Localconn)
	MongoDbRemote = db.NewMongoDbRemote(config.Remoteconn)

	return &Application{config}
}

func processFlags() {

	flag.StringVar(&Options.ProductionFlag, "production", "false", "When in production mode uses other config file variables (default false)")
	flag.StringVar(&Options.TemplateMode, "tplmode", "true", "Template mode means you can only log in by sending a post, and a number of other options")
	flag.Parse()
}

func parseConfig() Config {
	var err error
	var raw []byte
	if Options.ProductionFlag == "true" {
		raw, err = ioutil.ReadFile("/opt/config.json")
	} else {
		raw, err = ioutil.ReadFile("config.json")
	}
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	c := Config{}
	json.Unmarshal(raw, &c)

	return c
}

func (app Application) InitService() {
	app.ResetBuffer() //includes a full update
	app.CreateFiatCache()

	go app.ContinousService()
}

func (app Application) ContinousService() {
	for true {
		app.UpdateFiatCache()
		app.UpdateCoinListCache()
		app.UpdateCols()
		// app.UpdatePortfolio(300)
		time.Sleep(time.Duration(5) * time.Minute)
	}
}

func (app Application) UpdateCoinListCache() {

	u := MongoDbRemote.GetRemoteCoinList()
	MongoDbLocal.UpdateCoinListCache(u)
}

func (app Application) CreateCollections(u []string) {

	for _, s1 := range u {
		app.CreateCollection(s1)
	}
}

func (app Application) CreateCollection(u string) error {

	var err error

	var coll_info mgo.CollectionInfo
	coll_info.Capped = true
	coll_info.MaxDocs = MAX_BUFF_SIZE
	coll_info.MaxBytes = MAX_DOCUMENT_SIZE_BYTES

	// fmt.Println(coll_info.Capped)

	err = MongoDbLocal.GetDb().C(u).Create(&coll_info)
	if err != nil {
		fmt.Println("Problem creating collection", u)
		// log.Fatalln(err)
		return err
	}

	index := mgo.Index{
		Key:        []string{"lastupdated"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	err = MongoDbLocal.GetDb().C(u).EnsureIndex(index)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Collection Created")
	return err
}

func (app Application) DropCollection(u string) {

	err := MongoDbLocal.GetDb().C(u).DropCollection()
	if err != nil {
		fmt.Println(u)
		fmt.Println("Collection Not Deleted")
		log.Fatalln(err)
	}
}

func (app Application) DropCollections(u []string) {

	num := 0
	for _, s := range u {
		app.DropCollection(s)
		num += 1
	}
	fmt.Println(num, " - Collections dropped")
}

// TO DO: Update this function so it doesn't pull all of the data everytime. Should just pull what it doesn't have
func (app Application) FullUpdateCols() {

	v := MongoDbLocal.ScanDatabase()

	for _, s := range v {
		app.FullUpdateCol(s, true)
	}
}

func (app Application) FullUpdateCol(name string, reset bool) {

	// t_now := time.Now().Unix()
	// t_check := t_now - 48*60*60
	// t_48_hrs_ago := t_check //grab 48 hours worth, just incase there has been any server downtime

	if DEBUG == true {
		MAX_BUFF_SIZE = 288
	}

	fmt.Println("fuck")
	b := []models.CcMarketValueNew{}

	pipeline := []bson.M{
		{"$match": bson.M{"timestamp": bson.M{"$gt": 0}}},
		{"$sort": bson.M{"timestamp": -1}},
		{"$unwind": "$coins"},
		{"$match": bson.M{"coins.id": name}},
		{"$limit": MAX_BUFF_SIZE},
		{
			"$group": bson.M{
				"_id":   "$_id",
				"coins": bson.M{"$push": "$coins"},
			}},
	}

	pipe := MongoDbRemote.GetDb().C("coins").Pipe(pipeline)
	err := pipe.All(&b)
	if err != nil {
		fmt.Printf("Error retriviing documents, %v", err)
	}

	// err := MongoDbRemote.GetDb().C("coins").Find(bson.M{"timestamp": bson.M{"$gt": t_48_hrs_ago}}).Select(bson.M{"coins": bson.M{"$elemMatch": bson.M{"id": name}}}).Sort("-timestamp").Limit(MAX_BUFF_SIZE).All(&b)
	// err := MgoDbCal.C("coins").Find(bson.M{}).All(&b)
	// iter := c.Find(bson.M{"price": bson.M{"$gt": 40}}).Iter()
	if err != nil {
		log.Fatalln(err)
	}
	fmt.Println(name)
	b_new := models.ConvertMarketValueNewOld(b)
	fmt.Println(len(b))

	if DEBUG == true {
		fmt.Println(b_new[0].LastUpdated)
		fmt.Println(b_new[1].LastUpdated)
		fmt.Println(b_new[2].LastUpdated)
		fmt.Println("....")
		fmt.Println(b_new[len(b_new)-2].LastUpdated)
		fmt.Println(b_new[len(b_new)-1].LastUpdated)
	}

	// fmt.Print(b[2].Name)
	num_docs_inserted := 0
	if reset == true {
		_, _ = MongoDbLocal.GetDb().C(name).RemoveAll(bson.M{})
		// TODO: UPDATE PROPERLY to INSErT OLDESt FIRST
	}

	for i := len(b_new) - 1; i >= 0; i-- {
		_ = MongoDbLocal.GetDb().C(name).Insert(b_new[i])
		num_docs_inserted += 1
	}
	fmt.Println(num_docs_inserted, " Documents inserted")
}

func (app Application) UpdateCols() {

	v := MongoDbLocal.ScanDatabase()
	for _, s := range v {
		app.FullUpdateCol(s, false)
	}
}

func (app Application) UpdateCol(name string) {
	t_now := time.Now().Unix()
	t_24_hrs_ago := int(t_now - 24*60*60)

	t_check := t_now - 48*60*60
	t_48_hrs_ago := t_check

	local_length, _ := MongoDbLocal.GetDb().C(name).Find(bson.M{"lastupdated": bson.M{"$gt": t_24_hrs_ago}}).Count()

	doc_to_insert := MAX_BUFF_SIZE - local_length
	if DEBUG == true {
		fmt.Println("Local DB Col Length:", local_length)
		fmt.Println("Docs that need to be inserted:", doc_to_insert)
	}

	if doc_to_insert > 0 {
		b := []models.CcMarketValueNew{}
		err := MongoDbRemote.GetDb().C("coins").Find(bson.M{"timestamp": bson.M{"$gt": t_48_hrs_ago}}).Select(bson.M{"coins": bson.M{"$elemMatch": bson.M{"id": name}}}).Sort("-timestamp").Limit(MAX_BUFF_SIZE).All(&b)
		// err := MgoDbCal.C("coins").Find(bson.M{}).All(&b)
		// iter := c.Find(bson.M{"price": bson.M{"$gt": 40}}).Iter()
		if err != nil {
			log.Fatalln(err)
		}
		fmt.Println(name)
		b_new := models.ConvertMarketValueNewOld(b)
		fmt.Println(len(b))

		if DEBUG == true {
			// fmt.Println(b[0].LastUpdated)
			// fmt.Println(b[1].LastUpdated)
			// fmt.Println(b[2].LastUpdated)
			// fmt.Println("......")
			// fmt.Println(b[doc_to_insert-2].LastUpdated)
			// fmt.Println(b[doc_to_insert-1].LastUpdated)

		}

		// for _, t := range b {
		for i := doc_to_insert - 1; i >= 0; i-- {
			_ = MongoDbLocal.GetDb().C(name).Insert(b_new[i])
		}
	}

	fmt.Println(doc_to_insert, " Documents inserted")
}

func (app Application) ResetBuffer() {

	//drop all collections (except for transactions)
	v := MongoDbLocal.ScanDatabase()
	app.DropCollections(v)

	u := MongoDbLocal.ScanTransactions()
	v = MongoDbLocal.ScanDatabase()

	//create the new collections, find the elements that are in transactions but are not in database
	diff1 := arrayhelpers.Difference(u, v) //difference between the two sets,
	//   diff = helpers.Difference(diff,v) //the difference between the two sets, but only looking at within the database
	diff2 := arrayhelpers.Difference(v, u)

	fmt.Println(diff1)
	fmt.Println(diff2)

	app.CreateCollections(diff1)

	//delete old collections, find the elements that are in database but are not in transactions
	app.DropCollections(diff2)

	app.FullUpdateCols()
}

//**// HTTP STUFF FOR THE BUFFER

func MakeResetHandler(app *Application) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		// w.Header() .Set("Access-Control-Allow-Origin","*")
		if Options.TemplateMode == "false" {
			w.Header().Set("Content-Type", "application/json")
		}
		fmt.Println("waiting for pass signal")
		app.ResetBuffer()

		w.WriteHeader(http.StatusOK)
	}
}

func MakeUpdateColsHandler(app *Application) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		// w.Header() .Set("Access-Control-Allow-Origin","*")
		if Options.TemplateMode == "false" {
			w.Header().Set("Content-Type", "application/json")
		}
		fmt.Println("waiting for pass signal")

		app.UpdateCols()

		w.WriteHeader(http.StatusOK)
	}
}

func MakeCreateColHandler(app *Application) httprouter.Handle {
	return func(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
		if Options.TemplateMode == "false" {
			w.Header().Set("Content-Type", "application/json")
		}

		body, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			fmt.Println("error reading in json from login")
		}

		var t CollectionCreateMarshaller
		err = json.Unmarshal(body, &t)
		if err != nil {
			fmt.Println(t.Name)
			panic(err)
		}
		defer req.Body.Close()
		//   un = t.Username
		//   pw = t.Password

		name := t.Name
		if Options.ProductionFlag == "false" {
			fmt.Println(name)
		}

		v := MongoDbLocal.ScanDatabase()
		fmt.Println(v)

		remotecoinlist := MongoDbRemote.GetRemoteCoinNames()

		if innie, _ := arrayhelpers.InArray(name, remotecoinlist); innie == true {
			if in, _ := arrayhelpers.InArray(name, v); in == false {
				err = app.CreateCollection(name)
				if err != nil {
					if Options.ProductionFlag == "false" {
						fmt.Println("Error with creating collection in the ring buffer")
					}

				}
			}
		} else {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		go app.UpdateCol(name)

		w.WriteHeader(http.StatusOK)
	}
}

func MakeDeleteColHandler(app *Application) httprouter.Handle {
	return func(w http.ResponseWriter, req *http.Request, p httprouter.Params) {
		if Options.TemplateMode == "false" {
			w.Header().Set("Content-Type", "application/json")
		}

		body, err := ioutil.ReadAll(req.Body)
		defer req.Body.Close()
		if err != nil {
			fmt.Println("error reading in json from login")
		}

		var t CollectionCreateMarshaller
		err = json.Unmarshal(body, &t)
		if err != nil {
			fmt.Println(t.Name)
			panic(err)
		}
		defer req.Body.Close()
		//   un = t.Username
		//   pw = t.Password

		name := t.Name
		if Options.ProductionFlag == "false" {
			fmt.Println(name)
		}

		trans := MongoDbLocal.ScanTransactions()
		buffer := MongoDbLocal.ScanDatabase()

		if in, _ := arrayhelpers.InArray(name, buffer); in == true {
			if in, _ := arrayhelpers.InArray(name, trans); in == false {

				app.DropCollection(name)
				fmt.Println(name)
				fmt.Println("Ringbuffer collection deleted")
			}
		}

		w.WriteHeader(http.StatusOK)
	}
}

func (app Application) CreateFiatCache() error {

	var err error

	var coll_info mgo.CollectionInfo
	coll_info.Capped = true
	coll_info.MaxDocs = MAX_FIAT_BUFF_SIZE
	coll_info.MaxBytes = MAX_DOCUMENT_SIZE_BYTES

	// fmt.Println(coll_info.Capped)
	err = MongoDbLocal.GetDb().C("fiat").DropCollection()
	if err != nil {
		fmt.Println("error deleting the fiat collection")
	}

	err = MongoDbLocal.GetDb().C("fiat").Create(&coll_info)
	if err != nil {
		fmt.Println("Problem creating collection")
		fmt.Println(err)
		return err
	}

	index := mgo.Index{
		Key:        []string{"timestamp"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	err = MongoDbLocal.GetDb().C("fiat").EnsureIndex(index)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println("Fiat Collection Created")
	return err
}

func (app Application) UpdateFiatCache() {
	fiatobject := MongoDbRemote.GetFiatMostRecentValue()
	btcusdvalue := MongoDbRemote.GetValueBTCNow()
	fiatobject.Btcusdvalue = btcusdvalue

	fmt.Println(btcusdvalue)
	fmt.Println(fiatobject.Btcusdvalue)
	MongoDbLocal.GetDb().C("fiat").Insert(fiatobject)
}
